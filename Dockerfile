FROM node:14-alpine

RUN apk add --no-cache make gcc g++ py-pip

RUN mkdir -p /data/node_modules
WORKDIR /data/
ENV PATH /data/node_modules/.bin:$PATH

COPY yarn.lock .
COPY package.json .


RUN rm -rf node_modules

RUN yarn install

COPY . .

EXPOSE 3000

CMD [ "yarn", "start" ]