const dotenv = require('dotenv');
dotenv.config();

const IS_DEV = process.env.NODE_ENV == 'development';

module.exports = {
  parser: '@typescript-eslint/parser',
  extends: [
    'airbnb-typescript',
    'airbnb/hooks',
    'prettier',
    'plugin:import/errors',
    'plugin:import/warnings',
    'plugin:import/typescript',
  ],
  parserOptions: {
    project: './tsconfig.json',
  },
  plugins: ['prettier', 'autofix'],
  rules: {
    'autofix/no-debugger': 'error',
    'prettier/prettier': 'error',
    'import/no-unresolved': 0,
    'import/no-cycle': 0,
    'react/prop-types': 0,
    'react-hooks/exhaustive-deps': 0,
    'react/jsx-props-no-spreading': 0,
    'react-hooks/rules-of-hooks': 0,
    "no-unused-vars": "off",
    "@typescript-eslint/no-unused-vars": ["error"],
    'no-console': IS_DEV ? 0 : 'error',
  },
  settings: {
    'import/resolver': {
      node: {
        extensions: ['.ts', '.tsx'],
      },
    },
  },
};
