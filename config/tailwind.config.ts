/* eslint-disable import/no-extraneous-dependencies */
import path from 'path';
import dotenv from 'dotenv';
import {isNil, omitBy} from 'lodash';

const defaultConfig = require('tailwindcss/defaultConfig');
const defaultTheme = require('tailwindcss/defaultTheme');

const defaultColors = omitBy(require('tailwindcss/colors'), 'lightBlue');

const env = dotenv.config().parsed;

const colors = {
  ...omitBy(defaultColors, 'lightBlue'),
  transparent: 'transparent',
  primary: '#1F4E5A',
  'primary-alt': '#029C8E',
  secondary: '#FFA658',
  'secondary-alt': '#FFDB69',
  tertiary: 'EA5F40',
  backdrop: 'rgba(0, 0, 0, 0.6)',
};

const theme = {
  ...defaultTheme,
  colors,
  container: {
    center: true,
    padding: '2rem',
  },
  fontFamily: {
    archivo: 'Archivo, sans-serif',
  },
  fontSize: {
    ...defaultTheme.fontSize,
    default: ['1rem', '1.5rem'],
    label: ['1.11rem', '1.20rem'],
    content: ['1.22rem', '1.33rem'],
    button: ['1.27rem', '1.39rem'],
    heading: ['1.66rem', '1.81rem'],
    highlight: ['1.77rem', '1.93rem'],
    title: ['1.88rem', '2.055rem'],
  },
};

const variants = {
  extend: {
    backgroundColor: ['hover', 'even', 'odd'],
    borderColor: ['hover', 'focus', 'even', 'odd'],
    textColor: ['hover', 'focus', 'even', 'odd'],
  },
};

const purge = {
  enabled: env?.NODE_ENV !== 'development',
  content: [
    path.join(process.cwd(), 'src/client/**/*.tsx'),
    path.join(process.cwd(), 'src/client/**/*.ts'),
    path.join(process.cwd(), 'src/client/**/*.scss'),
    path.join(process.cwd(), 'src/client/**/*.sass'),
    path.join(process.cwd(), 'src/client/**/*.css'),
  ],
};

const tailwindConfig = {
  ...defaultConfig,
  mode: 'jit',
  darkMode: 'media',
  theme,
  variants,
  purge,
};

export default omitBy(tailwindConfig, isNil);
