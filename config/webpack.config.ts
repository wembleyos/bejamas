import webpack, {Configuration} from 'webpack';
import path from 'path';
import dotenv from 'dotenv';

import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import TerserPlugin from 'terser-webpack-plugin';
import CssMinimizerPlugin from 'css-minimizer-webpack-plugin';
import CompressionPlugin from 'compression-webpack-plugin';
import FriendlyErrorsWebpackPlugin from 'friendly-errors-webpack-plugin';
import {InjectManifest} from 'workbox-webpack-plugin';
import {omit} from 'lodash';

import postcssOptions from './postcss.config';

dotenv.config();

const IS_DEVELOPMENT = process.env.NODE_ENV === 'development';

const plugins = [
  IS_DEVELOPMENT && new webpack.HotModuleReplacementPlugin(),
  new webpack.NoEmitOnErrorsPlugin(),
  new MiniCssExtractPlugin(),
  !IS_DEVELOPMENT && new HtmlWebpackPlugin(),
  new CssMinimizerPlugin(),
  !IS_DEVELOPMENT &&
    new InjectManifest({
      swSrc: '/sw.ts',
    }),
  new webpack.DefinePlugin({
    'process.env': JSON.stringify(
      omit(dotenv.config().parsed, ['SECRET', 'REDIS_HOST']),
    ),
  }),
  new FriendlyErrorsWebpackPlugin({
    clearConsole: true,
  }),
  !IS_DEVELOPMENT &&
    new CompressionPlugin({
      algorithm: 'gzip',
      test: /\.js$|\.css$|\.html$/,
      threshold: 10240,
      minRatio: 0.8,
    }),
].filter(Boolean) as webpack.WebpackPluginInstance[];

const optimization: Configuration['optimization'] = {
  removeEmptyChunks: true,
  removeAvailableModules: !IS_DEVELOPMENT,
  flagIncludedChunks: !IS_DEVELOPMENT,
  concatenateModules: !IS_DEVELOPMENT,
  mangleExports: !IS_DEVELOPMENT,
  minimize: true,
  minimizer: [
    new TerserPlugin({
      test: /\.(js)(\?.*)?$/i,
    }) as any,
  ],
};
const performance: Configuration['performance'] = {
  hints: false,
};

const config: webpack.Configuration = {
  module: {
    rules: [
      {
        enforce: 'pre',
        test: /\.(ts|tsx)$/,
        exclude: /node_modules/,
        loader: 'eslint-loader',
      },
      {
        test: /\.(css|s[ac]ss)$/i,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          {
            loader: 'postcss-loader',
            options: {postcssOptions},
          },
          'sass-loader',
        ],
      },
      {
        test: /\.(js|ts|tsx)$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
        },
      },
      {
        test: /\.svg$/,
        use: ['@svgr/webpack'],
      },
    ],
  },
  optimization,
  performance,
  context: path.join(process.cwd(), 'src/client'),
  devtool: IS_DEVELOPMENT ? 'eval-source-map' : 'hidden-nosources-source-map',
  watch: false,
  entry: [
    'regenerator-runtime/runtime',
    IS_DEVELOPMENT && 'webpack-hot-middleware/client',
    IS_DEVELOPMENT && 'react-hot-loader/patch',
    !IS_DEVELOPMENT && 'sw.ts',
    'index',
  ].filter(Boolean) as string[],
  resolve: {
    extensions: ['.js', '.ts', '.tsx', '.json', 'css', 'scss', 'sass'],
    modules: [
      path.join(process.cwd(), 'node_modules'),
      path.join(process.cwd(), 'src/client'),
    ],
    alias: {
      'react-dom': '@hot-loader/react-dom',
      '@components': path.resolve(process.cwd(), 'src/client/components/'),
      '@hoc': path.resolve(process.cwd(), 'src/client/hoc/'),
      '@assets': path.resolve(process.cwd(), 'src/assets/'),
      '@providers': path.resolve(process.cwd(), 'src/client/providers/'),
      '@store': path.resolve(process.cwd(), 'src/client/store/'),
      '@typings': path.resolve(process.cwd(), 'typings'),
      '@actions': path.resolve(process.cwd(), 'src/client/store/actions/'),
      '@pages': path.resolve(process.cwd(), 'src/client/pages/'),
      '@queries': path.resolve(process.cwd(), 'src/client/queries/'),
      '@modules': path.resolve(process.cwd(), 'src/client/modules/'),
    },
  },
  output: {
    publicPath: '/',
  },
  plugins,
  mode: (process.env.NODE_ENV as any) || 'production',
};

export default config;
