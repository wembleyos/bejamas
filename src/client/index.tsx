import React from 'react';
import {render} from 'react-dom';
import {renderRoutes} from 'react-router-config';
import {BrowserRouter} from 'react-router-dom';

import 'styles/index.scss';
import {QueryClient, QueryClientProvider, setLogger} from 'react-query';
import {Hydrate} from 'react-query/hydration';
import routes from './routes';

if (process.env.NODE_ENV === 'production') {
  setLogger({
    log: () => {},
    warn: () => {},
    error: () => {},
  });
}
// eslint-disable-next-line no-underscore-dangle
const dehydratedState = (window as any).__REACT_QUERY_STATE__;

const queryClient = new QueryClient();

render(
  <QueryClientProvider client={queryClient}>
    <Hydrate state={dehydratedState}>
      <BrowserRouter>{renderRoutes(routes)}</BrowserRouter>
    </Hydrate>
  </QueryClientProvider>,
  document.querySelector('#app'),
);
