import React from 'react';

const Chevron: React.FC<React.SVGProps<SVGSVGElement>> = (props) => (
  <svg
    width={20}
    height={13}
    viewBox="0 0 20 13"
    xmlns="http://www.w3.org/2000/svg"
    fill="none"
    {...props}>
    <path d="M2 2l8 8 8-8" stroke="currentColor" strokeWidth={3} />
  </svg>
);

export default Chevron;
