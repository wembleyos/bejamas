import React from 'react';

const CloseIcon: React.FC<React.SVGProps<SVGSVGElement>> = ({
  className,
  onClick,
}) => (
  <svg
    width={22}
    height={22}
    viewBox="0 0 22 22"
    fill="none"
    className={className}
    onClick={onClick}
    xmlns="http://www.w3.org/2000/svg">
    <path d="M2 2l18 18M2 20L20 2" stroke="currentColor" strokeWidth={4} />
  </svg>
);

export default CloseIcon;
