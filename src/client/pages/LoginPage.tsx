import {setAuthenticate} from '@actions';
import {useTranslate} from '@providers';
import {login, createMutation} from '@queries';
import {useStore} from '@store';
import {User, Credentials} from '@typings';
import React, {useEffect, useState} from 'react';
import {Link, useHistory} from 'react-router-dom';

const LoginPage: React.FC = () => {
  const {dispatch} = useStore();

  const [credentials, setCredentials] = useState<Credentials>({
    email: '',
    password: '',
  });
  const [validity, setValidity] = useState<ValidityState>(null);
  const {mutate, data, error, isSuccess} = createMutation<User, Credentials>(
    'login',
    login,
  );

  const {translate} = useTranslate();
  const history = useHistory();

  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setCredentials({
      ...credentials,
      [e.target.name]: e.target.value,
    });

    setValidity(e.target.validity);
  };

  const onSubmit = (e: React.ChangeEvent<HTMLFormElement>) => {
    e.preventDefault();
    if (!validity?.valid) {
      return;
    }
    mutate(credentials);
  };

  useEffect(() => {
    if (data) {
      dispatch(setAuthenticate(data));
    }
  }, [data]);

  useEffect(() => {
    if (isSuccess) {
      history.push('/');
    }
  }, [isSuccess]);

  return (
    <div className="w-full lg:w-4/12 md:w-1/2 md:mx-auto flex items-center h-full">
      <div className="w-full p-8 bg-white dark:bg-gray-900 shadow-xl">
        <h3 className="text-heading mb-8 font-bold text-center">
          {translate('headers.login')}
        </h3>
        {error && (
          <div className="flex items-center justify-center p-4 border w-full border-red-200 text-red-500 mb-8 bg-white dark:bg-black">
            {translate(`errors.${error.response?.data?.message}`)}
          </div>
        )}
        <form onSubmit={onSubmit} noValidate>
          <label htmlFor="email">
            <p>{translate('placeholders.email')}</p>
            <input
              name="email"
              id="email"
              type="email"
              onChange={onChange}
              value={credentials?.email}
              required
            />
          </label>

          <label htmlFor="password">
            <p>{translate('placeholders.password')}</p>
            <input
              name="password"
              id="password"
              type="password"
              onChange={onChange}
              value={credentials?.password}
              required
              minLength={6}
            />
          </label>

          <div className="flex flex-row flex-wrap mt-8">
            <button
              type="submit"
              className="primary flex-1"
              aria-label={translate('actions.login')}
              disabled={!validity?.valid}>
              {translate('actions.login')}
            </button>
          </div>
        </form>

        <div className="mt-8 flex items-center justify-center">
          {translate('others.no_account')}?
          <Link to="/register" className="ml-2">
            {translate('actions.register')}
          </Link>
        </div>
      </div>
    </div>
  );
};

export default LoginPage;
