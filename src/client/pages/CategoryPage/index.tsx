import {setCategory, setCategorySearch} from '@actions';
import {createQuery, getCategory} from '@queries';
import {useStore} from '@store';
import React, {useEffect} from 'react';
import {useRouteMatch} from 'react-router';

import {hot} from 'react-hot-loader';
import {CategoryMatch} from '@typings';
import ErrorPage from '@pages/ErrorPage';

import {Helmet} from 'react-helmet';
import CategoryLayout from './components/CategoryLayout';
import ProductList from './components/ProductList';

const CategoryPage: React.FC = () => {
  const match = useRouteMatch<CategoryMatch>();
  const {dispatch, category} = useStore();

  const {data, refetch, isError, isFetched, error} = createQuery(
    `category`,
    getCategory(match.params, category.search),
  );

  useEffect(() => {
    dispatch(setCategory(data));
  }, [data, category.current]);

  useEffect(() => {
    dispatch(setCategorySearch({paginate: 1}));
  }, [category.search.category]);

  useEffect(() => {
    refetch();
  }, [category.search, match]);

  if (isError && !data?.id && isFetched) {
    return <ErrorPage statusCode={error.response?.status} />;
  }

  return (
    <>
      <Helmet>
        <title>
          {[data?.parent?.name, data?.name].filter(Boolean).join(' / ')}
        </title>
      </Helmet>
      <CategoryLayout data={data} refetch={() => refetch()}>
        <ProductList refetch={() => refetch()} data={data?.products || []} />
      </CategoryLayout>
    </>
  );
};

export default hot(module)(CategoryPage);
