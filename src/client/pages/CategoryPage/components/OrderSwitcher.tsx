import {setCategorySearch} from '@actions';
import Select from '@components/Select';
import {useTranslate} from '@providers';
import {useStore} from '@store';
import React, {useEffect, useState} from 'react';
import OrderIcon from '../../../assets/OrderIcon';

const OrderSwitcher: React.FC = () => {
  const {translate} = useTranslate();
  const {category, dispatch} = useStore();

  const [state, setState] = useState<string>('ASC');

  const orderValues = [
    {name: translate('others.ascending').toLowerCase(), value: 'asc'},
    {name: translate('others.descending').toLowerCase(), value: 'desc'},
  ];

  useEffect(() => {
    dispatch(setCategorySearch({price_order: state?.toUpperCase()}));

    return () => {
      dispatch(setCategorySearch({price_order: 'asc'}));
    };
  }, [state]);

  return (
    <div className="flex flex-row flex-wrap">
      <div className="flex items-center mr-4">
        <OrderIcon className="mr-2" />
        <span className="text-gray-500 font-light">
          {translate('placeholders.sortBy')}
        </span>
      </div>
      <div className="w-24">
        <Select
          values={orderValues}
          value={state || (category.search?.price_order as string)}
          placeholder={translate('placeholders.price')}
          onChange={(select) => setState(select)}
        />
      </div>
    </div>
  );
};

export default OrderSwitcher;
