import {setCategorySearch} from '@actions';
import {useTranslate} from '@providers';
import {useStore} from '@store';
import React, {useEffect, useState} from 'react';
import {FiltersPriceType} from '@typings';
import {useLocation} from 'react-router';

const PriceFilters: React.FC = () => {
  const {translate} = useTranslate();
  const [state, setState] = useState<FiltersPriceType>(null);
  const {dispatch, category} = useStore();
  const location = useLocation();

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setState({
      ...state,
      [e.target.name]: e.target.value,
    });
  };

  useEffect(() => {
    dispatch(setCategorySearch(state));
  }, [state]);

  useEffect(() => {
    setState(null);
  }, [location.pathname]);

  return (
    <div className="pb-8 border-b-2 dark:border-gray-900">
      <h3 className="font-bold mb-8 dark:text-gray-400">
        {translate('placeholders.price')}
      </h3>
      <div className="flex flex-row flex-wrap dark:text-gray-500">
        <div className="flex-1 mr-4">
          <input
            type="number"
            name="price_from"
            onChange={handleChange}
            value={state?.price_from || category.search.price_from || ''}
            placeholder={translate('placeholders.from')}
          />
        </div>
        <div className="flex-1">
          <input
            type="number"
            name="price_to"
            onChange={handleChange}
            value={state?.price_to || category.search.price_to || ''}
            placeholder={translate('placeholders.to')}
          />
        </div>
      </div>
    </div>
  );
};

export default PriceFilters;
