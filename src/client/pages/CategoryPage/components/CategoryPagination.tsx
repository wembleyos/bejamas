import {setCategorySearch} from '@actions';
import {useStore} from '@store';
import React, {useEffect, useState} from 'react';

const CategoryPagination: React.FC = () => {
  const {dispatch, category} = useStore();
  const [paginate, setCurrentPage] = useState(1);

  const total = Math.ceil(category.current.total / 6);
  const pages = Array.from(new Array(total).keys()).map((item) => item + 1);

  useEffect(() => {
    dispatch(setCategorySearch({paginate}));
  }, [paginate]);

  useEffect(() => {
    setCurrentPage((category.search.paginate || 1) as unknown as number);
  }, [category.search.paginate]);

  return (
    <div className="w-full flex items-center justify-center my-8">
      <div className="w-64 flex flex-row flex-wrap items-center justify-center md:justify-between">
        {paginate !== 1 && (
          <div
            className="mr-4 w-1/12 p-2 cursor-pointer text-xl"
            role="presentation"
            onClick={() => setCurrentPage(Number(paginate) - 1)}>
            ‹
          </div>
        )}
        {pages.map((page) => (
          <div
            key={page}
            className={[
              'opacity-50 p-2 cursor-pointer',
              page === paginate && 'opacity-100 dark:text-white',
              page !== paginate && 'dark:text-gray-600',
            ]
              .filter(Boolean)
              .join(' ')}
            role="presentation"
            onClick={() => setCurrentPage(page)}>
            {page}
          </div>
        ))}
        {paginate < total && (
          <div
            className="ml-4 w-1/12 p-2 cursor-pointer text-xl"
            role="presentation"
            onClick={() => setCurrentPage(Number(paginate) + 1)}>
            ›
          </div>
        )}
      </div>
    </div>
  );
};

export default CategoryPagination;
