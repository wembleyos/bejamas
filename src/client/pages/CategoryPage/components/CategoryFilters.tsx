import {setCategorySearch} from '@actions';
import RestrictedArea from '@components/RestrictedArea';
import {useTranslate} from '@providers';
import {useStore} from '@store';
import {CategoryMatch, DeleteCategoryArgs} from '@typings';
import React, {useEffect, useState} from 'react';
import {useLocation, useRouteMatch} from 'react-router';
import {Link} from 'react-router-dom';

import {TiDelete} from 'react-icons/ti';
import {createMutation, deleteCategoryMutation} from '@queries';

interface CategoryFiltersProps {
  refetch: () => void;
}

const CategoryFilters: React.FC<CategoryFiltersProps> = ({refetch}) => {
  const [selected, setSelected] = useState<string[]>([]);
  const {translate} = useTranslate();
  const {category, dispatch, config} = useStore();
  const location = useLocation();
  const match = useRouteMatch<CategoryMatch>();

  const {mutate, isSuccess} = createMutation<unknown, DeleteCategoryArgs>(
    'deleteCategory',
    deleteCategoryMutation,
  );

  const onSelect = (id: string) => {
    if (selected.includes(String(id))) {
      setSelected(selected.filter((item) => item !== String(id)));
    } else {
      setSelected(selected.concat(String(id)));
    }
  };

  const handleDelete = async (categoryId: string) => {
    await mutate({categoryId});
  };

  useEffect(() => {
    if (isSuccess) {
      refetch();
    }
  }, [isSuccess]);

  useEffect(() => {
    dispatch(setCategorySearch({category: selected}));
  }, [selected]);

  useEffect(() => {
    setSelected([]);
  }, [location.pathname]);

  return (
    <div className="pb-8 border-b-2 mb-4 dark:border-gray-900">
      <h3 className="font-bold mb-8 dark:text-gray-400">
        {translate('placeholders.category')}
      </h3>
      <div className="text-label">
        {[
          !category.current?.parent && category.current?.children,
          !match.params.category && config.categories,
        ]
          .flat()
          .filter(Boolean)
          .map(({slug, name, id}) => (
            <div key={slug} className="mb-2 flex flex-row flex-wrap">
              <Link
                to={`${match.url}/${slug}`.replace(/([^:]\/)\/+/g, '$1')}
                className="dark:text-gray-600 dark:hover:text-gray-400">
                {name}
              </Link>
              <RestrictedArea scope="admin">
                <span
                  className="ml-auto cursor-pointer"
                  onClick={() => handleDelete(id)}
                  role="presentation">
                  <TiDelete />
                </span>
              </RestrictedArea>
            </div>
          ))}
        {category.current?.parent &&
          category?.current?.children?.map(({id, name}) => (
            <div
              className="flex flex-row flex-wrap items-center justify-start mb-4 select-none cursor-pointer dark:text-gray-600 dark:hover:text-gray-400"
              key={id}>
              <input
                type="checkbox"
                value={id}
                className="mr-4"
                defaultChecked={selected.includes(id)}
                onClick={() => onSelect(id)}
              />
              <p>{name}</p>
              <RestrictedArea scope="admin">
                <span
                  className="ml-auto cursor-pointer"
                  onClick={() => handleDelete(id)}
                  role="presentation">
                  <TiDelete />
                </span>
              </RestrictedArea>
            </div>
          ))}
      </div>
    </div>
  );
};

export default CategoryFilters;
