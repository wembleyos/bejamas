import {addToCart} from '@actions';
import RestrictedArea from '@components/RestrictedArea';

import loadable from '@loadable/component';
import {useTranslate} from '@providers';
import {useStore} from '@store';
import {Product} from '@typings';
import React, {useEffect, useState} from 'react';
import {Link} from 'react-router-dom';

const AdminProductTileIndicators = loadable(
  () => import('@components/admin/ProductTileIndicators'),
);

interface ProductTileProps extends Product {
  refetch: () => void;
}

const ProductTile: React.FC<ProductTileProps> = (product) => {
  const {category, name, price, images, id, bestseller, refetch, subtitle} =
    product;
  if (!id) {
    return null;
  }

  const {translate} = useTranslate();
  const [isHovered, setHover] = useState(false);
  const {dispatch} = useStore();

  const categoryLink = [
    '/catalog',
    category?.parent?.slug,
    category?.slug,
  ].filter(Boolean);

  useEffect(() => {}, []);

  const onSelect = () => {
    dispatch(addToCart(product));
  };

  return (
    <div className="product-tile select-none">
      <div
        className="image-wrapper mb-3"
        role="presentation"
        onMouseEnter={() => setHover(true)}
        onMouseLeave={() => setHover(false)}>
        <div
          className="absolute top-0 left-0 w-full h-full flex items-center bg-center bg-cover bg-no-repeat"
          style={images?.length && {backgroundImage: `url(${images[0]?.url})`}}>
          <div className="relative w-full h-full flex">
            {bestseller && (
              <span className="p-2 bg-white dark:bg-black absolute left-0 top-0 text-black dark:text-white inline-block mr-auto">
                {translate('placeholders.bestseller')}
              </span>
            )}
            <RestrictedArea scope="admin">
              <AdminProductTileIndicators
                product={product}
                onDelete={() => refetch()}
              />
            </RestrictedArea>
            {isHovered && (
              <div className="w-full mt-auto relative">
                <button
                  type="button"
                  className="primary w-full"
                  aria-label={translate('actions.add_to_cart')}
                  onClick={onSelect}>
                  {translate('actions.add_to_cart')}
                </button>
              </div>
            )}
          </div>
        </div>
      </div>

      <Link
        to={`${categoryLink.join('/')}`}
        className="text-gray-500 font-bold">
        {category?.name}
      </Link>
      <p className="text-heading font-bold mb-2">{name}</p>
      {subtitle && <p className="opacity-50 mb-2">{subtitle}</p>}
      <p className="text-gray-500 mb-2">
        {price?.currency.name} {price?.value}
      </p>
    </div>
  );
};
export default ProductTile;
