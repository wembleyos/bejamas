import {useStore} from '@store';
import React, {createRef, useEffect} from 'react';
import {Category} from '@typings';
import {useLocation} from 'react-router';
import Breadcrumbs from './Breadcrumbs';
import HightLightedProduct from './HighlightedProduct';
import OrderSwitcher from './OrderSwitcher';
import CategoryMenu from './Menu';
import CategoryPagination from './CategoryPagination';

interface CategoryLayoutProps {
  children: React.ReactElement | React.ReactElement[];
  data: Category;
  refetch: () => void;
}

const CategoryLayout: React.FC<CategoryLayoutProps> = ({
  children,
  data,
  refetch,
}) => {
  const {category, user} = useStore();
  const scrollRef = createRef<HTMLDivElement>();
  const {pathname} = useLocation();

  useEffect(() => {
    if (typeof window === 'undefined' || !scrollRef.current) {
      return;
    }

    window.scrollTo({
      top: scrollRef.current.offsetTop - 150,
      behavior: 'smooth',
    });
  }, [pathname]);

  return (
    <div>
      <div className="mb-10">
        {(data?.featured ||
          user.data?.role?.some((role) => role.name === 'admin')) && (
          <HightLightedProduct product={data?.featured} />
        )}
        <div className="flex flex-col md:flex-row flex-wrap items-center justify-between pt-12">
          {category?.current && (
            <div className="mr-auto">
              <Breadcrumbs />
            </div>
          )}
          {!!category.current?.products?.length && (
            <div className="mt-8 md:mt-0 ml-auto">
              <OrderSwitcher />
            </div>
          )}
        </div>
      </div>
      <div className="flex flex-col md:flex-row flex-wrap" ref={scrollRef}>
        <aside className="hidden md:inline-block md:w-1/4 mr-12">
          <CategoryMenu refetch={refetch} />
        </aside>
        <section className="flex-1 pb-10">
          {children}
          {!!category.current?.total && <CategoryPagination />}
        </section>
      </div>
    </div>
  );
};

export default CategoryLayout;
