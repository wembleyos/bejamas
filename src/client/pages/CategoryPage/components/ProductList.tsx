import {useStore} from '@store';
import {Product} from '@typings';
import React from 'react';
import AddProductTile from '../../../components/admin/AddProductTile';
import ProductTile from './ProductTile';

interface ProductListProps {
  refetch: () => void;
  data: Product[];
}

const ProductList: React.FC<ProductListProps> = ({refetch, data}) => {
  const {category} = useStore();

  return (
    <div className="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3 gap-8">
      {category.current && <AddProductTile onSuccess={() => refetch()} />}
      {data.map((product) => (
        <ProductTile {...product} key={product.id} refetch={refetch} />
      ))}
    </div>
  );
};

export default ProductList;
