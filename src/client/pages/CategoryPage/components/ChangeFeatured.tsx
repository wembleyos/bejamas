import RestrictedArea from '@components/RestrictedArea';
import {useTranslate} from '@providers';
import {changePotd, createMutation, createQuery, getFeatured} from '@queries';
import {useStore} from '@store';
import {Product} from '@typings';
import React from 'react';

interface ChangeFeaturedProps {
  onSelect: (product: Product) => void;
}

const ChangeFeatured: React.FC<ChangeFeaturedProps> = ({onSelect}) => {
  const {translate} = useTranslate();
  const {category} = useStore();

  const {data} = createQuery(
    'featuredProducts',
    getFeatured(category.current?.id),
  );

  const {mutate, data: newProduct} = createMutation('changePotd', changePotd);

  const handleSelect = async (product: Product) => {
    await mutate({
      product: product.id,
      category: category.current?.id,
    });
    onSelect(newProduct);
  };

  const handleClear = async () => {
    await mutate({
      product: null,
      category: category.current?.id,
    });
    onSelect(null);
  };

  return (
    <RestrictedArea>
      <div className="p-4">
        <h3 className="font-bold mb-2">{translate('headers.change_potd')}</h3>
        <ul className="grid grid-cols-3 md:grid-cols-4 gap-4 my-8">
          <div
            className="pb-48 relative opacity-50 hover:opacity-100 transition ease-in-out cursor-pointer"
            role="presentation"
            onClick={handleClear}>
            <div className="absolute top-0 left-0 w-full h-full bg-black dark:bg-white flex items-center justify-center font-bold text-white dark:bg-white dark:text-black">
              {translate('actions.clear')}
            </div>
          </div>
          {data?.map((product) => (
            <div
              className="pb-48 relative opacity-50 hover:opacity-100 transition ease-in-out cursor-pointer"
              key={product.id}
              role="presentation"
              onClick={() => handleSelect(product)}>
              {!!product.images.length && (
                <div
                  className="absolute top-0 left-0 w-full h-full bg-black dark:bg-white bg-cover bg-center bg-cover bg-no-repeat"
                  style={{backgroundImage: `url(${product.images[0]?.url})`}}
                />
              )}
            </div>
          ))}
        </ul>
      </div>
    </RestrictedArea>
  );
};

export default ChangeFeatured;
