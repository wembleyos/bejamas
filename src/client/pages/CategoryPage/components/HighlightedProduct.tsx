import {useTranslate} from '@providers';
import React, {useState} from 'react';
import {useStore} from '@store';
import {addToCart, setPotd} from '@actions';
import RestrictedArea from '@components/RestrictedArea';
import Modal from '@components/Modal';
import {Link} from 'react-router-dom';
import {Product} from '@typings';
import ChangeFeatured from './ChangeFeatured';

interface HighlightedProduct {
  product: Product;
}

const HightLightedProduct: React.FC<HighlightedProduct> = ({product}) => {
  const {translate} = useTranslate();
  const {dispatch, cart, category} = useStore();
  const [isPotdModal, togglePotdModal] = useState(false);

  const isProductInCart = cart.products.some((item) => item.id === product?.id);

  const handleSelect = (potd: Product) => {
    dispatch(setPotd(potd));
    togglePotdModal(false);
  };

  const categoryLink = [category.current?.parent?.slug, category.current?.slug]
    .filter(Boolean)
    .join('/');

  return (
    <>
      <RestrictedArea scope="admin">
        {isPotdModal && (
          <Modal isOpen={isPotdModal} onClose={() => togglePotdModal(false)}>
            <ChangeFeatured onSelect={handleSelect} />
          </Modal>
        )}
      </RestrictedArea>
      <article className="product-highlight">
        <div className="flex flex-row flex-wrap items-center justify-between mb-10">
          <h1 className="text-highlight capitalize font-bold">
            {product?.name}
          </h1>
          {!isProductInCart ||
            (product && (
              <div className="hidden md:inline-block">
                <button
                  className="primary"
                  type="button"
                  aria-label={translate('actions.add_to_cart')}
                  onClick={() => dispatch(addToCart(product))}>
                  {translate('actions.add_to_cart')}
                </button>
              </div>
            ))}
        </div>
        <div className="bg-black dark:bg-white relative flex product-highlight-bg mb-8 overflow-hidden">
          {product && (
            <div
              className="absolute w-full h-full top-0 left-0 bg-black dark:bg-white bg-cover bg-no-repeat bg-center"
              style={{
                backgroundImage: `url(${product.images[0]?.url})`,
              }}
            />
          )}
          <div className="absolute bottom-0 left-0">
            <div className="flex flex-row flex-wrap items-center">
              <div className="px-12 py-4 font-bold text-label bg-white text-black dark:bg-black dark:text-white">
                {translate('placeholders.potd')}
              </div>
              <RestrictedArea scope="admin">
                <>
                  <button
                    type="button"
                    className="bg-secondary py-4 font-bold text-label text-black dark:text-white"
                    aria-label={translate('actions.change')}
                    onClick={() => togglePotdModal(true)}>
                    {translate('actions.change')}
                  </button>
                </>
              </RestrictedArea>
            </div>
          </div>
        </div>
        {product && (
          <div className="flex flex-col md:flex-row flex-wrap items-between border-b-2 pb-12 dark:border-gray-900">
            <div className="md:w-8/12">
              <h2 className="font-bold text-content mb-4">
                {translate('others.single_about')}{' '}
                <span className="capitalize">{product?.name}</span>
              </h2>
              <Link
                to={`/catalog/${categoryLink}`}
                className="text-content font-bold mb-4 text-gray-500 capitalize">
                {product?.category?.name || category.current?.parent?.name}
              </Link>
              <p className="font-light mb-4 whitespace-pre-line text-gray-500 md:pr-8">
                {product?.description}
                {console.log(product)}
              </p>
            </div>
            <div className="sm:ml-auto flex-1 text-right flex flex-col flex-wrap items-between">
              {product.recommendations?.length && (
                <div className="mb-12">
                  <h3 className="text-content font-bold mb-12">
                    {translate('headers.also_buy')}
                  </h3>
                  <div className="flex flex-row flex-wrap">
                    {category.current?.recommendations?.map((recommended) => (
                      <Link
                        to={`/category/${recommended.category?.id}`}
                        className="flex-1 mr-8 bg-black dark:bg-white pb-32 md:pb-32 last:mr-0 bg-cover bg-center"
                        key={recommended.id}
                        style={{
                          backgroundImage: `url(${recommended.images[0].url})`,
                        }}
                      />
                    ))}
                  </div>
                </div>
              )}

              {product.details && (
                <div className="mt-auto">
                  <h3 className="text-content font-bold mb-2">
                    {translate('headers.details')}
                  </h3>
                  <p className="font-light mb-4 whitespace-pre-line text-gray-500">
                    {product?.details}
                  </p>
                </div>
              )}
            </div>
          </div>
        )}
      </article>
    </>
  );
};

export default HightLightedProduct;
