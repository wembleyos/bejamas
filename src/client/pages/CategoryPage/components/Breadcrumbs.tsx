import {useStore} from '@store';
import React from 'react';
import {Link, useRouteMatch} from 'react-router-dom';

const Breadcrumbs: React.FC = () => {
  const {
    category: {current},
  } = useStore();
  const match = useRouteMatch();
  const slug = [...match.url.trim().split('/').filter(Boolean)].filter(
    (item) => item !== current?.slug,
  );

  return (
    <div className="flex flex-row flex-wrap items-center text-heading capitalize">
      {!current.parent && <h3 className="font-semibold">{current.name}</h3>}
      {current.parent && (
        <Link to={`/${slug.join('/')}`} className="font-semibold">
          {current?.parent?.name || current.name || 'category'}
        </Link>
      )}
      {current.parent && (
        <>
          <span className="ml-4 mr-2">/</span>
          <span className="text-gray-500">{current?.name}</span>
        </>
      )}
    </div>
  );
};

export default Breadcrumbs;
