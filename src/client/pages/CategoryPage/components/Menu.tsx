import {setCategorySearch} from '@actions';
import {useStore} from '@store';
import React from 'react';
import CategoryOptionsWrapper, {
  OptionsSelectPayload,
} from '@components/OptionsWrapper';
import NewCategoryModal from '@components/admin/NewCategoryModal';
import CategoryFilters from './CategoryFilters';
import PriceFilters from './PriceFilters';

interface CategoryMenuProps {
  refetch: () => void;
}

const CategoryMenu: React.FC<CategoryMenuProps> = ({refetch}) => {
  const {category, dispatch, user} = useStore();

  const onOptionsSelect = (data: OptionsSelectPayload) => {
    dispatch(setCategorySearch(data));
  };

  return (
    <div>
      <CategoryFilters refetch={refetch} />
      {user.data?.id && <NewCategoryModal refetch={refetch} />}
      {category.current?.id && (
        <CategoryOptionsWrapper
          data={category.current.options}
          onSelect={onOptionsSelect}
          select
          refetch={refetch}
        />
      )}
      <PriceFilters />
    </div>
  );
};

export default CategoryMenu;
