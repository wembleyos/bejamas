import AddressForm from '@components/forms/AddressForm';
import {useTranslate} from '@providers';
import {
  createMutation,
  createQuery,
  saveAddress,
  getActiveAdresssQuery,
} from '@queries';
import {useStore} from '@store';
import {Address} from '@typings';
import {isEqual} from 'lodash';
import React, {useState, useEffect} from 'react';

export interface CheckoutFormPayload {
  billingAddress: Address;
  shippingAddress: Address;
}

interface CheckoutFormProps {
  onChange: (data: CheckoutFormPayload) => void;
}

const CheckoutForm: React.FC<CheckoutFormProps> = ({onChange}) => {
  const {translate} = useTranslate();
  const {user} = useStore();

  const {data} = createQuery('getUserAddress', getActiveAdresssQuery);
  const {mutate} = createMutation('setDefaultAddress', saveAddress);

  const [shippingAddress, setShippingAddress] = useState<Address>(data);
  const [billingAddress, setBillingAddress] = useState<Address>(null);

  const [isActive, setActive] = useState(false);
  const [isSameAsShipping, setSameAsShipping] = useState(true);

  const handleActive = () => {
    setActive(true);
    setShippingAddress({
      ...shippingAddress,
      isActive,
    });

    mutate({...shippingAddress, isActive: true});
  };

  useEffect(() => {
    onChange({billingAddress, shippingAddress});
  }, [billingAddress, shippingAddress]);

  useEffect(() => {
    setShippingAddress(data);
  }, [data]);

  useEffect(() => {
    if (isSameAsShipping) {
      setBillingAddress(data);
    } else {
      setBillingAddress(null);
    }
  }, [isSameAsShipping]);

  return (
    <div>
      <div className="mb-8">
        <div className="flex flex-row flex-wrap items-center justify-between">
          <h3 className="text-heading font-bold mb-4">
            {translate('headers.shipping_address')}
          </h3>
          {user.data?.id && !isEqual(data, shippingAddress) && data.id && (
            <span
              className="ml-auto text-xs opacity-50 hover:opacity-100 cursor-pointer"
              role="presentation"
              onClick={() => handleActive()}>
              {translate('actions.set_as_default')}
            </span>
          )}

          {user.data?.id && !data && (
            <span
              className="ml-auto text-xs opacity-50 hover:opacity-100 cursor-pointer"
              role="presentation"
              onClick={() => handleActive()}>
              {translate('actions.save')}
            </span>
          )}
        </div>
        <AddressForm
          onChange={setShippingAddress}
          data={data}
          type="shipping"
        />
      </div>

      <h3 className="text-heading font-bold mb-2">
        {translate('headers.billing_address')}
      </h3>

      <div className="flex flex-row flex wrap items-center mb-4">
        <input
          type="checkbox"
          defaultChecked={isSameAsShipping}
          onClick={() => setSameAsShipping(!isSameAsShipping)}
          className="mr-4"
        />
        <p className="opacity-50">
          {translate('placeholders.same_as_shipping')}
        </p>
      </div>

      {!isSameAsShipping && (
        <div>
          <AddressForm
            onChange={setBillingAddress}
            data={billingAddress}
            type="billing"
          />
        </div>
      )}
    </div>
  );
};

export default CheckoutForm;
