import {useStore} from '@store';
import React, {useEffect} from 'react';
import CheckoutProductItem from './CheckoutProductItem';

const CheckoutProductsList: React.FC = () => {
  const {cart} = useStore();
  useEffect(() => {}, []);

  return (
    <ol className="w-full">
      {cart.products?.map((product) => (
        <li key={product.id} className="pb-4 mb-4 border-b-2 last:border-b-0">
          <CheckoutProductItem product={product} />
        </li>
      ))}
    </ol>
  );
};

export default CheckoutProductsList;
