import {useTranslate} from '@providers';
import {createMutation, placeOrderMutation} from '@queries';
import {useStore} from '@store';
import {PlaceOrderArgs} from '@typings';
import React, {useEffect, createRef, useState} from 'react';
import {hot} from 'react-hot-loader';
import CheckoutForm, {CheckoutFormPayload} from './CheckoutForm';
import CheckoutProductsList from './CheckoutProductsList';
import CheckoutSummary from './CheckoutSummary';

const CheckoutPage: React.FC = () => {
  const {translate} = useTranslate();
  const {cart} = useStore();
  const [state, setState] = useState<PlaceOrderArgs>(null);

  const scrollRef = createRef<HTMLDivElement>();
  const {mutate} = createMutation('placeOrder', placeOrderMutation);

  const handleFormChange = ({
    shippingAddress,
    billingAddress,
  }: CheckoutFormPayload) => {
    setState({
      shippingAddress,
      billingAddress,
      products: cart.products,
    });
  };

  const placeOrder = () => {
    mutate(state);
  };

  useEffect(() => {
    if (typeof window === 'undefined' || !scrollRef.current) {
      return;
    }

    window.scrollTo({
      top: scrollRef.current?.offsetTop - 100,
      behavior: 'smooth',
    });
  }, []);

  return (
    <div
      className="flex flex-row md:flex-col flex-wrap-reverse md:flex-wrap pb-10"
      ref={scrollRef}>
      <div className="grid grid-cols-1 md:grid-cols-2 gap-4">
        <div className="flex flex-col flex-wrap items-center justify-between w-full md:pr-4 md:border-r-2 md:w-full">
          <div className="w-full md:max-h-84 overflow-y-auto">
            <CheckoutProductsList />
          </div>
          <div className="mt-auto ml-auto">
            <CheckoutSummary />
            <button
              className="primary ml-auto"
              type="button"
              disabled
              onClick={() => placeOrder()}>
              {translate('actions.place_order')}
            </button>
          </div>
        </div>
        <CheckoutForm onChange={handleFormChange} />
      </div>
    </div>
  );
};

export default hot(module)(CheckoutPage);
