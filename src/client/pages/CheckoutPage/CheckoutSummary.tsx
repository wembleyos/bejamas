import {useTranslate} from '@providers';
import {useStore} from '@store';
import React, {useEffect} from 'react';

const CheckoutSummary: React.FC = () => {
  const {
    config,
    cart: {products},
  } = useStore();
  const {translate} = useTranslate();

  const total =
    products?.length &&
    products
      .map((item) => Number(item.price?.value))
      ?.reduce((current, next) => Number(current) + Number(next));

  useEffect(() => {}, []);

  return (
    <div className="ml-auto">
      <div className="flex flex-row flex-wrap text-right items-center">
        <p className="font-bold text-title mb-2">
          {translate('order.summary')}
        </p>
        <div className="flex flex-row flex-wrap text-right ml-4">
          <span className="font-bold">{total || 0}</span>
          <span className="ml-2">{config.currency?.name}</span>
        </div>
      </div>
    </div>
  );
};

export default CheckoutSummary;
