import {Product} from '@typings';
import React, {useEffect} from 'react';
import {Link} from 'react-router-dom';

interface CheckoutProductItemProps {
  product: Product;
}

const CheckoutProductItem: React.FC<CheckoutProductItemProps> = ({product}) => {
  const {category} = product;

  useEffect(() => {}, []);
  const categoryLink = [
    '/catalog',
    category?.parent?.slug,
    category?.slug,
  ].filter((item) => item && item !== category?.slug);

  return (
    <span className="flex flex-row flex-wrap items-center product-tile w-full">
      <span className="w-1/4 mr-8 h-32 bg-black dark:bg-white relative">
        <span
          className="absolute top-0 left-0 w-full h-full bg-cover bg-center"
          style={
            product.images?.length && {
              backgroundImage: `url(${product.images[0].url})`,
            }
          }
        />
      </span>

      <span className="flex-1">
        <span className="text-title mb-2 font-bold block">{product.name}</span>
        <Link
          to={`${categoryLink.join('/')}`.replace(/\/+/g, '/')}
          className="text-gray-500 font-bold">
          {category?.name}
        </Link>

        {product.subtitle && (
          <span className="text-title mb-2 block text-gray-500">
            {product.subtitle}
          </span>
        )}

        {product.price && (
          <span className="text-label block text-gray-400 dark:text-gray-800">
            {product.price?.value}
            <span className="ml-2">{product.price?.currency.name}</span>
          </span>
        )}
      </span>
    </span>
  );
};

export default CheckoutProductItem;
