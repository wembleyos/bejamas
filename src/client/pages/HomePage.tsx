import React, {useEffect} from 'react';
import {Redirect} from 'react-router';

const HomePage: React.FC = () => {
  useEffect(() => {}, []);
  return (
    <div>
      <Redirect to="/catalog" />
    </div>
  );
};

export default HomePage;
