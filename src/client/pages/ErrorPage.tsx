import React from 'react';
import {Route} from 'react-router';

interface ErrorPageProps {
  statusCode?: number;
  children?: React.ReactElement | React.ReactElement[] | string;
}

const ErrorPage: React.FC<ErrorPageProps> = ({statusCode, children}) => (
  <Route
    render={({staticContext}) => {
      if (staticContext) {
        staticContext.statusCode = statusCode || 500;
      }

      return (
        <div className="h-full flex items-center justify-center">
          <h3 className="text-title mb-8 text-red-500">
            {staticContext?.statusCode || statusCode || 500}
          </h3>
          {children}
        </div>
      );
    }}
  />
);

export default ErrorPage;
