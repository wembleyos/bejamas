import {useTranslate} from '@providers';
import {createMutation, register} from '@queries';
import {RegisterData} from '@typings';
import React, {useState} from 'react';
import {Link} from 'react-router-dom';

const RegisterPage: React.FC = () => {
  const {translate} = useTranslate();

  const [state, setState] = useState<RegisterData>(null);
  const [validity, setValidity] = useState<ValidityState>(null);

  const {mutate, error, isSuccess} = createMutation<boolean, RegisterData>(
    'register',
    register,
  );

  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setState({
      ...state,
      [e.target.name]: e.target.value,
    });

    setValidity(e.target.validity);
  };

  const onSubmit = async (e: React.ChangeEvent<HTMLFormElement>) => {
    e.preventDefault();
    await mutate(state);
  };

  return (
    <div className="w-full md:w-1/3 md:mx-auto h-full flex items-center">
      <div className="w-full p-8 bg-white dark:bg-gray-900 shadow-xl">
        {error && (
          <div className="p-4 text-center text-red-500 border border-red-200 mb-8">
            {translate(`errors.${error?.response?.data.message}`)}
          </div>
        )}
        <h3 className="text-heading mb-8 font-bold text-center">
          {translate(
            isSuccess ? 'headers.account_created' : 'headers.create_account',
          )}
        </h3>

        {isSuccess && (
          <p className="text-center text-green-500">
            {translate('messages.succesful_register')}
          </p>
        )}

        {!isSuccess && (
          <form onSubmit={onSubmit} noValidate>
            <label htmlFor="email">
              <p>{translate('placeholders.email')}</p>
              <input
                type="email"
                name="email"
                id="email"
                value={state?.email || ''}
                onChange={onChange}
                required
              />
            </label>

            <label htmlFor="firstName">
              <p>{translate('placeholders.firstName')}</p>
              <input
                type="text"
                name="firstName"
                id="firstName"
                value={state?.firstName || ''}
                onChange={onChange}
                minLength={3}
                required
              />
            </label>

            <div className="flex flex-row flex-wrap mb-4">
              <label htmlFor="password" className="flex-1 mr-4">
                <p>{translate('placeholders.password')}</p>
                <input
                  type="password"
                  name="password"
                  id="password"
                  value={state?.password || ''}
                  onChange={onChange}
                  minLength={6}
                  required
                />
              </label>

              <label htmlFor="confirmPassword" className="flex-1">
                <p>{translate('placeholders.confirmPassword')}</p>
                <input
                  type="password"
                  name="confirmPassword"
                  id="confirmPassword"
                  value={state?.confirmPassword || ''}
                  onChange={onChange}
                  minLength={6}
                  required
                />
              </label>
            </div>
            <button
              className="primary"
              type="submit"
              aria-label={translate('actions.register')}
              disabled={!validity?.valid}>
              {translate('actions.register')}
            </button>
          </form>
        )}

        <div className="mt-8 text-center">
          {!isSuccess && translate('others.already_registered')}?
          <Link to="/login" className="ml-2">
            {translate('actions.login')}
          </Link>
        </div>
      </div>
    </div>
  );
};

export default RegisterPage;
