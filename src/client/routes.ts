import Root from '@components/Root';
import CategoryPage from '@pages/CategoryPage';
import HomePage from '@pages/HomePage';
import LoginPage from '@pages/LoginPage';
import RegisterPage from '@pages/RegisterPage';
import {getCategory} from '@queries';
import {CategoryMatch} from '@typings';
import {QueryClient} from 'react-query';
import {match} from 'react-router';
import {RouteConfig} from 'react-router-config';
import Loadable from '@loadable/component';

const CheckoutPage = Loadable(() => import('@pages/CheckoutPage'));
Loadable(() => import('@components/NullComponent'));

const routes: RouteConfig[] = [
  {
    component: Root,
    routes: [
      {
        path: '/login',
        component: LoginPage,
      },
      {
        path: '/register',
        component: RegisterPage,
      },
      {
        path: '/checkout',
        component: CheckoutPage,
      },
      {
        path: '/catalog/:category?/:subcategory?/:lastCategory?',
        component: CategoryPage,
        exact: true,
        loadData: async (
          routeMatch: match<CategoryMatch>,
          queryClient: QueryClient,
        ) => {
          try {
            queryClient.prefetchQuery(
              'category',
              getCategory(routeMatch.params),
            );
          } catch (e) {
            // errorHanlder
          }
        },
      },
      {
        path: '/',
        exact: true,
        component: HomePage,
      },
    ],
  },
];

export default routes;
