import {setConfig} from '@actions';
import {Category, Currency, Option} from '@typings';

export enum ConfigActions {
  SET_THEME = 'CONFIG/SET_THEME',
  SET_CONFIG = 'CONFIG/SET_CONFIG',
}

export type ConfigActionTypes = ReturnType<typeof setConfig>;

export interface ConfigState {
  currency: Currency;
  categories: Category[];
  options: Option[];
}

export const configState: ConfigState = {
  currency: null,
  categories: [],
  options: [],
};
