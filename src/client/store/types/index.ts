import {CartActionTypes, cartState} from './CartState';
import {CategoryActionTypes, categoryState} from './CategoryState';
import {ConfigActionTypes, configState} from './ConfigState';
import {UserActionTypes, userState} from './UserState';

export type ActionTypes =
  | ConfigActionTypes
  | UserActionTypes
  | CartActionTypes
  | CategoryActionTypes;

export interface Store {
  dispatch?: React.Dispatch<ActionTypes>;
  config: typeof configState;
  user: typeof userState;
  cart: typeof cartState;
  category: typeof categoryState;
}

export const rootState: Store = {
  config: configState,
  user: userState,
  cart: cartState,
  category: categoryState,
};

export * from './CartState';
export * from './CategoryState';
export * from './ConfigState';
export * from './UserState';
