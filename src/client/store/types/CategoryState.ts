import {clearCategorySearch, setCategory, setCategorySearch} from '@actions';
import {Category, CategorySearchParams} from '@typings';
import setPotd from '../actions/category/setPotd';

interface CategoryState {
  totalPages: number;
  current?: Category;
  search: CategorySearchParams;
}

export type CategoryActionTypes = ReturnType<
  | typeof setCategory
  | typeof setCategorySearch
  | typeof clearCategorySearch
  | typeof setPotd
>;

export enum CategoryActions {
  SET_CATEGORY = 'CATEGORY/SET_CATEGORY',
  SET_HIGHLIGHTED = 'CATEGORY/SET_HIGHLIGHTED',
  SET_CURRENT = 'CATEGORY/SET_CURRENT_PAGE',
  SET_PRODUCTS = 'CATEGORY/SET_PRODUCTS',
  SET_SEARCH = 'CATEGORY/SET_SEARCH',
  CLEAR_SEARCH = 'CATEGORY/CLEAR_SEARCH',
  SET_POTD = 'CATEGORY/SET_POTD',
}

export const categoryState: CategoryState = {
  totalPages: 1,
  search: {
    price_order: 'ASC',
    paginate: null,
    category: [],
  },
  current: null,
};
