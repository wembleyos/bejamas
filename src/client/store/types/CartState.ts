import {addToCart, clearCart, toggleMinicart} from '@actions';
import {Product} from '@typings';

export type CartActionTypes = ReturnType<
  typeof toggleMinicart | typeof clearCart | typeof addToCart
>;

export enum CartActions {
  ADD_TO_CART = 'CART/ADD_TO_CART',
  REMOVE_FROM_CART = 'CART/REMOVE_FROM_CART',
  INCREASE_QTY = 'CART/INCREASE_QUANTITY',
  TOGGLE_CART = 'CART/OPEN_CART',
  CLEAR_CART = 'CART/CLEAR_CART',
}

interface CartState {
  products: Product[];
  isOpened: boolean;
}

export const cartState: CartState = {
  products: [],
  isOpened: false,
};
