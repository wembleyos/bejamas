import {setAuthenticate} from '@actions';
import {User} from '@typings';

export enum UserActions {
  SET_AUTHENTICATE = 'USER/SET_AUTHENTICATE',
}

export type UserActionTypes = ReturnType<typeof setAuthenticate>;

interface UserState {
  data: User;
}

export const userState: UserState = {
  data: null,
};
