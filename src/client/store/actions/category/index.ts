import setCategory from './setCategory';
import setCategorySearch from './setCategorySearch';
import clearCategorySearch from './clearCategorySearch';
import setPotd from './setPotd';

export {setCategory, setCategorySearch, clearCategorySearch, setPotd};

export default {
  setCategory,
  setCategorySearch,
  clearCategorySearch,
  setPotd,
};
