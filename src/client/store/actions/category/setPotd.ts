import {CategoryActions, Product} from '@typings';

interface SetPotdAction {
  type: CategoryActions.SET_POTD;
  payload: Product;
}

const setPotd = (payload: Product): SetPotdAction => ({
  type: CategoryActions.SET_POTD,
  payload,
});

export default setPotd;
