import {CategoryActions} from '@typings';

interface ClearCategorySearchAction {
  type: CategoryActions.CLEAR_SEARCH;
}

const clearCategorySearch = (): ClearCategorySearchAction => ({
  type: CategoryActions.CLEAR_SEARCH,
});

export default clearCategorySearch;
