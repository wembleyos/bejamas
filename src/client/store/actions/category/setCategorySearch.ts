import {CategoryActions} from '@typings';

type CategorySearchPayload = Record<string, any>;

interface SetCategorySearchAction {
  type: CategoryActions.SET_SEARCH;
  payload: CategorySearchPayload;
}

const setCategorySearch = (
  payload: CategorySearchPayload,
): SetCategorySearchAction => ({
  type: CategoryActions.SET_SEARCH,
  payload,
});

export default setCategorySearch;
