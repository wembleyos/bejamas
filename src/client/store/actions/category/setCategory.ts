import {Category, CategoryActions} from '@typings';

interface SetCategoryAction {
  type: CategoryActions.SET_CATEGORY;
  payload: Category;
}

const setCategory = (payload: Category): SetCategoryAction => ({
  type: CategoryActions.SET_CATEGORY,
  payload,
});

export default setCategory;
