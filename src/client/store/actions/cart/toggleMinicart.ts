import {CartActions} from '@typings';

interface ToggleMinicartAction {
  type: CartActions.TOGGLE_CART;
  isOpened: boolean;
}

const toggleMinicart = (isOpened?: boolean): ToggleMinicartAction => ({
  type: CartActions.TOGGLE_CART,
  isOpened,
});

export default toggleMinicart;
