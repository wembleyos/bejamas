import toggleMinicart from './toggleMinicart';
import clearCart from './clearCart';
import addToCart from './addToCart';

export {toggleMinicart, clearCart, addToCart};

export default {
  toggleMinicart,
  clearCart,
  addToCart,
};
