import {CartActions, Product} from '@typings';

interface AddToCartAction {
  type: CartActions.ADD_TO_CART;
  product: Product;
}

const addToCart = (product: Product): AddToCartAction => ({
  type: CartActions.ADD_TO_CART,
  product,
});

export default addToCart;
