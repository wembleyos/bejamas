import {CartActions} from '@typings';

interface ClearCartAction {
  type: CartActions.CLEAR_CART;
}

const clearCart = (): ClearCartAction => ({
  type: CartActions.CLEAR_CART,
});

export default clearCart;
