import {ConfigActions, ConfigState} from '@typings';

interface SetConfigPayload {
  type: ConfigActions.SET_CONFIG;
  payload: ConfigState;
}
const setConfig = (payload: ConfigState): SetConfigPayload => ({
  type: ConfigActions.SET_CONFIG,
  payload,
});

export default setConfig;
