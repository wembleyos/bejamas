import setConfig from './setConfig';

export * from './user';
export * from './cart';
export * from './category';

export {setConfig};

export default {setConfig};
