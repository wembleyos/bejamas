import {User, UserActions} from '@typings';

interface SetAuthenticateAction {
  type: UserActions.SET_AUTHENTICATE;
  payload: User;
}

const setAuthenticate = (payload: User): SetAuthenticateAction => ({
  type: UserActions.SET_AUTHENTICATE,
  payload,
});

export default setAuthenticate;
