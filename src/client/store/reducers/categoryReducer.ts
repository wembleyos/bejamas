import {CategoryActions, CategoryActionTypes, categoryState} from '@typings';

const categoryReducer: React.Reducer<
  typeof categoryState,
  CategoryActionTypes
> = (state, action) => {
  switch (action.type) {
    case CategoryActions.SET_POTD: {
      return {
        ...state,
        current: {
          ...state.current,
          featured: action.payload,
        },
      };
    }
    case CategoryActions.CLEAR_SEARCH: {
      return {
        ...state,
        search: categoryState.search,
      };
    }
    case CategoryActions.SET_SEARCH: {
      return {
        ...state,
        search: {...state.search, ...action.payload},
      };
    }
    case CategoryActions.SET_CATEGORY: {
      return {
        ...state,
        current: action.payload,
      };
    }
    default:
      return state;
  }
};

export default categoryReducer;
