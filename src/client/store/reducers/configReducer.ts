import {ConfigActions, ConfigActionTypes, ConfigState} from '@typings';

const configReducer: React.Reducer<ConfigState, ConfigActionTypes> = (
  state,
  action,
) => {
  switch (action.type) {
    case ConfigActions.SET_CONFIG: {
      return {...state, ...action.payload};
    }
    default:
      return state;
  }
};

export default configReducer;
