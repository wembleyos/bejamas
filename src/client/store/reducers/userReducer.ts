import {UserActions, UserActionTypes, userState} from '@typings';

const userReducer: React.Reducer<typeof userState, UserActionTypes> = (
  state,
  action,
) => {
  switch (action.type) {
    case UserActions.SET_AUTHENTICATE:
      return {
        ...state,
        data: action.payload,
      };
    default:
      return state;
  }
};

export default userReducer;
