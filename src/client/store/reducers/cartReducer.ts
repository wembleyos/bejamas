import {CartActions, CartActionTypes, cartState} from '@typings';

const cartReducer: React.Reducer<typeof cartState, CartActionTypes> = (
  state,
  action,
) => {
  switch (action.type) {
    case CartActions.ADD_TO_CART: {
      const products = [...new Set([...state.products, action.product])].filter(
        (product) => product?.id,
      );

      return {
        ...state,
        isOpened: true,
        products,
      };
    }
    case CartActions.CLEAR_CART:
      return {
        ...state,
        products: [],
      };
    case CartActions.TOGGLE_CART:
      return {
        ...state,
        isOpened: !state.isOpened,
      };
    default:
      return state;
  }
};

export default cartReducer;
