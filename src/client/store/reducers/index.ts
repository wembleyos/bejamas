import {
  ActionTypes,
  CartActionTypes,
  CategoryActionTypes,
  ConfigActionTypes,
  Store,
  UserActionTypes,
} from '@typings';

import cartReducer from './cartReducer';
import configReducer from './configReducer';
import userReducer from './userReducer';
import categoryReducer from './categoryReducer';

const mainReducer = (
  {config, user, cart, category}: Store,
  action: ActionTypes,
) => ({
  config: configReducer(config, action as ConfigActionTypes),
  user: userReducer(user, action as UserActionTypes),
  cart: cartReducer(cart, action as CartActionTypes),
  category: categoryReducer(category, action as CategoryActionTypes),
});

export default mainReducer;
