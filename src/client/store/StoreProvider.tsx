import {rootState} from '@typings';
import React, {useReducer} from 'react';
import mainReducer from './reducers';

export const StoreContext = React.createContext(rootState);

interface StoreProviderProps {
  children: React.ReactElement;
}

const StoreProvider: React.FC<StoreProviderProps> = ({children}) => {
  // const sessionStore =
  //   typeof window !== 'undefined' &&
  //   (JSON.parse(sessionStorage.getItem('store')) as unknown as Store);

  const [store, dispatch] = useReducer(mainReducer, rootState);

  // useEffect(() => {
  //   if (typeof window !== 'undefined') {
  //     sessionStorage.setItem('store', JSON.stringify(store));
  //   }
  // }, [store]);

  return React.useMemo(
    () => (
      <StoreContext.Provider value={{...store, dispatch}}>
        {children}
      </StoreContext.Provider>
    ),
    [store],
  );
};

export default StoreProvider;
