import {useContext} from 'react';

import StoreProvider, {StoreContext} from './StoreProvider';

export const useStore = () => useContext(StoreContext);

export default StoreProvider;
