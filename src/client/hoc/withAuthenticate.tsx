import React, {ComponentType} from 'react';
import {useStore} from '@store';
import {Roles} from '@typings';
import {Redirect, Route} from 'react-router';

function withAuthenticate<P>(Component: ComponentType<P>, role?: Roles) {
  return (props: P) => {
    const {user} = useStore();

    if (!user.data?.role || user.data?.role !== role) {
      return (
        <Route
          render={({staticContext}) => {
            if (staticContext) {
              staticContext.statusCode = 403;
            }
            return <Redirect to="/login" />;
          }}
        />
      );
    }

    return <Component {...props} />;
  };
}

export default withAuthenticate;
