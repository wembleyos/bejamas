import withAuthenticate from './withAuthenticate';

export {withAuthenticate};

export default {
  withAuthenticate,
};
