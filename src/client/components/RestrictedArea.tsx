import {useStore} from '@store';
import {Role} from '@typings';
import React from 'react';

interface RestrictedAreaProps {
  scope?: Role['name'];
  children: React.ReactElement;
}

const RestrictedArea: React.FC<RestrictedAreaProps> = ({children, scope}) => {
  const {user} = useStore();

  if (!user.data) {
    return null;
  }

  if (scope && user.data?.role?.some((role) => role.name === scope)) {
    return children;
  }

  if (scope && user) {
    return null;
  }

  if (user) {
    return children;
  }

  return null;
};

export default RestrictedArea;
