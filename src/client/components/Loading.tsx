import {useTranslate} from '@providers';
import React from 'react';

const Loading: React.FC = () => {
  const {translate} = useTranslate();

  return (
    <div>
      <h3 className="text-title font-bold mb-8">
        {translate('others.loading')}
      </h3>
      <p>{translate('messages.loading')}</p>
    </div>
  );
};

export default Loading;
