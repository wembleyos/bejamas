import {clearCart, toggleMinicart} from '@actions';
import {useTranslate} from '@providers';
import {useStore} from '@store';
import React, {useEffect} from 'react';
import {useHistory, useLocation} from 'react-router';
import CloseIcon from '../assets/CloseIcon';

const Minicart: React.FC = () => {
  const {translate} = useTranslate();
  const {cart, dispatch} = useStore();
  const history = useHistory();
  const location = useLocation();

  const onClear = () => {
    dispatch(clearCart());
    dispatch(toggleMinicart());
  };

  const createOrder = () => {
    history.push('/checkout');
  };

  useEffect(() => {
    if (cart.isOpened) {
      dispatch(toggleMinicart());
    }
  }, [location.pathname]);

  const classNames = ['minicart', cart.isOpened && 'opened']
    .filter(Boolean)
    .join(' ');

  return (
    <div className="fixed z-20 right-0 top-0 mt-20 mr-2 md:mt-20 md:mr-24 select-none">
      <div className={classNames}>
        <CloseIcon
          className="ml-auto cursor-pointer hover:opacity-50"
          onClick={() => dispatch(toggleMinicart())}
        />
        {!!cart.products.length && (
          <div className="mb-8">
            {cart.products?.map((product) => (
              <div
                className="flex flex-row flex-wrap w-full border-b last:border-b-0 items-center justify-center py-4 dark:border-gray-800"
                key={product.id}>
                <div className="w-1/2 mr-4">
                  <h3 className="text-label font-bold mb-2 capitalize">
                    {product.name}
                  </h3>
                  {product.price && (
                    <p className="text-heading opacity-50">
                      <span className="font-bold">
                        {product.price?.currency.name}
                      </span>{' '}
                      {product.price?.value}
                    </p>
                  )}
                </div>
                <div className="flex-1">
                  <div className="product-image relative">
                    <div
                      className="bg-black dark:bg-white absolute top-0 left-0 h-full w-full bg-center bg-cover bg-no-repeat"
                      style={
                        product.images?.length && {
                          backgroundImage: `url(${product.images[0]?.url})`,
                        }
                      }
                    />
                  </div>
                </div>
              </div>
            ))}
          </div>
        )}
        {!cart.products.length && (
          <p className="text-center">{translate('messages.empty_basket')}</p>
        )}
        {!!cart.products.length && (
          <div className="flex flex-row flex-wrap">
            <button
              className="secondary flex-1 mr-4"
              type="button"
              aria-label={translate('actions.clear')}
              onClick={() => onClear()}>
              {translate('actions.clear')}
            </button>
            <button
              className="primary flex-1"
              type="button"
              aria-label={translate('actions.create_order')}
              onClick={() => createOrder()}>
              {translate('actions.create_order')}
            </button>
          </div>
        )}
      </div>
    </div>
  );
};

export default Minicart;
