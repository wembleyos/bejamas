import React, {useEffect, useState} from 'react';
import Chevron from '../assets/Chevron';

type SelectValue = {
  [key: string]: string;
};

interface SelectProps {
  className?: string;
  values: SelectValue[];
  onChange?: (value: string) => void;
  showValue?: boolean;
  value?: string;
  placeholder?: string | React.ReactElement;
  name?: string;
}

const Select: React.FC<SelectProps> = ({
  className,
  values,
  onChange,
  placeholder,
  showValue,
  value,
  name,
}) => {
  const classNames = [
    'relative flex items-center justify-between cursor-pointer text-light select-none flex-1',
  ]
    .concat(className)
    .join(' ');
  const [current, setCurrent] = useState(value);
  const [isOpened, toggleState] = useState(false);

  const onSelect = (selected: SelectValue) => {
    setCurrent(selected.value);
    toggleState(!isOpened);
    onChange(selected.value);
  };

  useEffect(() => {
    setCurrent(
      value ||
        values?.find((item) => item.value === value)?.value ||
        values[0]?.value,
    );
  }, [value]);

  useEffect(() => {
    if (current) {
      onChange(current);
    }
  }, [current]);

  const displayName =
    showValue && values.find((item) => item.value === value)?.name;
  return (
    <div className={classNames}>
      <div
        onClick={() => toggleState(!isOpened)}
        role="presentation"
        className="flex items-center justify-between flex-1 dark:text-white w-full h-full">
        <div className="flex-1 dark:text-gray-500">
          {(showValue && displayName) || name || placeholder || displayName}
        </div>
        <span
          style={{transform: `rotate(${isOpened ? 180 : -0}deg)`}}
          className="ml-2 transition ease-in-out">
          <Chevron />
        </span>
      </div>
      {isOpened && (
        <div className="absolute top-0 mt-8 z-10 w-full">
          <div className="relative w-full">
            <ul className="border bg-white border-black dark:border-gray-700 dark:bg-black dark:text-gray-700 max-h-44 overflow-y-auto mb-4 w-full">
              {[...new Set(values)].map((item) => (
                <div
                  key={item.value}
                  onClick={() => onSelect(item)}
                  className={[
                    'border-b border-gray-200 dark:border-gray-800 last:border-b-0 p-2 text-left hover:bg-black hover:text-white dark:hover:bg-white dark:hover:text-black w-full',
                  ]
                    .concat(
                      current === item.value &&
                        'bg-black text-white dark:bg-black dark:text-white',
                    )
                    .filter(Boolean)
                    .join(' ')}
                  role="presentation">
                  {item?.name}
                </div>
              ))}
            </ul>
          </div>
        </div>
      )}
    </div>
  );
};

export default Select;
