import React from 'react';

const NullComponent: React.FC = () => null;

export default NullComponent;
