import {Option} from '@typings';
import React, {useEffect, useState} from 'react';

import loadable from '@loadable/component';
import {useStore} from '@store';

import RestrictedArea from './RestrictedArea';
import {useAuth} from '../providers/AuthProvider';

const CategoryOptionsRemove = loadable(
  () => import('@components/admin/CategoryOptionsRemove'),
);

const CategoryOptionsSelect = loadable(
  () => import('./admin/CategoryOptionsSelect'),
);

export interface OptionsSelectPayload {
  [key: string]: string[];
}

interface OptionsWrapperProps {
  data: Option[];
  onSelect: (data: OptionsSelectPayload) => void;
  inline?: boolean;
  refetch?: () => void;
  select?: boolean;
}

const OptionsWrapper: React.FC<OptionsWrapperProps> = ({
  data,
  onSelect,
  inline,
  refetch,
  select,
}) => {
  const {category, config} = useStore();
  const {user} = useAuth();
  const [state, setState] = useState(
    Object.fromEntries(new Map(data.map((item) => [item.value, []]))),
  );

  const hash = new Date().getTime();

  const setSelected = (parent: string, item: string) => {
    const type = data.find(({id}) => id === parent).value;
    setState({
      ...state,
      [type]: state[type].includes(item)
        ? state[type].filter((el) => el !== item)
        : state[type].concat(item),
    });
  };

  useEffect(() => {
    onSelect(state);
  }, [state]);

  return (
    <>
      {data
        ?.map(
          (parent) =>
            !!parent.children?.length && (
              <div
                className="dark:text-gray-400 mb-8 select-none border-b-2 pb-8"
                key={parent.id}>
                <div className="flex flex-row flex-wrap items-center justify-between mb-8">
                  <p className="font-bold">{parent.name}</p>
                  {select && user?.data?.id && category.current && (
                    <div>
                      <CategoryOptionsRemove
                        refetch={refetch}
                        option={parent}
                        category={category.current}
                      />
                    </div>
                  )}
                </div>
                <div
                  className={[
                    typeof inline !== 'undefined' &&
                      inline &&
                      'flex flex-row flex-wrap items-center',
                  ]
                    .filter(Boolean)
                    .join(' ')}>
                  {parent.children?.map((children) => (
                    <label
                      className="mb-2 flex flex-row flex-wrap items-center cursor-pointer mr-8 last:mr-0"
                      htmlFor={`${parent.id}-${children.id}-${hash}`}
                      key={children.id}>
                      <input
                        type="checkbox"
                        name={parent.value}
                        className="mr-4"
                        defaultChecked={state[parent.value]?.includes(
                          children.id,
                        )}
                        onChange={() => setSelected(parent.id, children.id)}
                        id={`${parent.id}-${children.id}-${hash}`}
                      />
                      {children.name}
                    </label>
                  ))}
                </div>
              </div>
            ),
        )
        .filter(Boolean)}
      {category.current?.id &&
        category.current?.parent &&
        !!select &&
        !!config.options?.length && (
          <RestrictedArea scope="admin">
            <CategoryOptionsSelect
              refetch={refetch}
              selected={category?.current?.options}
            />
          </RestrictedArea>
        )}
    </>
  );
};

export default OptionsWrapper;
