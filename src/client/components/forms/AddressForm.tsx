import {useTranslate} from '@providers';
import {Address, AddressType} from '@typings';
import {isEqual} from 'lodash';
import React, {useEffect, useState} from 'react';

interface AddressFormProps {
  onChange: (data: Address) => void;
  data: Address;
  type: AddressType;
}

const AddressForm: React.FC<AddressFormProps> = ({data, onChange, type}) => {
  const [state, setState] = useState<Address>({...data, type});

  const {translate} = useTranslate();

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setState({
      ...state,
      [e.target.name]: e.target.value,
    });
  };

  useEffect(() => {
    setState({
      ...state,
      type,
    });
  }, [type]);

  useEffect(() => {
    if (!isEqual(state, data)) {
      onChange({...state, type});
    }
  }, [state]);

  useEffect(() => {
    if (!isEqual(data, state)) {
      setState(data);
    }
  }, [data]);

  return (
    <form>
      <div className="grid grid-cols-2 grid-gap-4">
        <div className="flex flex-row flex-wrap">
          <label htmlFor="firstName" className="w-1/2 mr-4">
            <p>{translate('placeholders.address.firstName')}</p>
            <input
              type="text"
              id="firstName"
              name="firstName"
              value={state?.firstName || ''}
              onChange={handleChange}
            />
          </label>

          <label className="flex-1" htmlFor="firstName">
            <p>{translate('placeholders.address.lastName')}</p>
            <input
              type="text"
              id="lastName"
              name="lastName"
              value={state?.lastName || ''}
              onChange={handleChange}
            />
          </label>
        </div>
      </div>

      <div>
        <div className="flex flex-row flex-wrap">
          <div className="w-1/2 mr-4">
            <label htmlFor="street">
              <p>{translate('placeholders.address.street')}</p>
              <input
                type="text"
                id="street"
                name="street"
                value={state?.street || ''}
                onChange={handleChange}
              />
            </label>
          </div>

          <div className="flex-1">
            <div className="flex flex-row flex-wrap w-full">
              <label htmlFor="houseNumber" className="w-1/2 mr-4">
                <p>{translate('placeholders.address.houseNumber')}</p>
                <input
                  type="text"
                  id="houseNumber"
                  name="houseNumber"
                  value={state?.houseNumber || ''}
                  onChange={handleChange}
                />
              </label>

              <label htmlFor="flatNumber" className="flex-1">
                <p>{translate('placeholders.address.flatNumber')}</p>
                <input
                  type="text"
                  id="flatNumber"
                  name="flatNumber"
                  value={state?.flatNumber || ''}
                  onChange={handleChange}
                />
              </label>
            </div>
          </div>
        </div>
      </div>

      <div className="flex flex-row flex-wrap">
        <div className="w-1/2 mr-4 flex flex-row flex-wrap">
          <label htmlFor="zipCode" className="mr-4 w-1/3">
            <p>{translate('placeholders.address.zipCode')}</p>
            <input
              type="text"
              id="zipCode"
              name="zipCode"
              value={state?.zipCode || ''}
              onChange={handleChange}
            />
          </label>

          <label htmlFor="city" className="flex-1">
            <p>{translate('placeholders.address.city')}</p>
            <input
              type="text"
              id="city"
              name="city"
              value={state?.city || ''}
              onChange={handleChange}
            />
          </label>
        </div>

        <div className="flex-1">
          <label htmlFor="phoneNumber">
            <p>{translate('placeholders.address.phoneNumber')}</p>
            <input
              type="tel"
              id="phoneNumber"
              name="phoneNumber"
              value={state?.phoneNumber || ''}
              onChange={handleChange}
            />
          </label>
        </div>
      </div>
    </form>
  );
};

export default AddressForm;
