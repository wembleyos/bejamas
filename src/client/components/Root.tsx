import StoreProvider from '@store';
import React from 'react';
import {hot} from 'react-hot-loader';
import {renderRoutes, RouteConfigComponentProps} from 'react-router-config';
import Bootstrap from './Bootstrap';

const Root: React.FC<RouteConfigComponentProps> = ({route}) => (
  <StoreProvider>
    <Bootstrap>{renderRoutes(route.routes)}</Bootstrap>
  </StoreProvider>
);

export default hot(module)(Root);
