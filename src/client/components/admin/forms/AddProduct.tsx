import Select from '@components/Select';
import {useTranslate} from '@providers';
import {createMutation, addProduct} from '@queries';
import {useStore} from '@store';
import {Product, ProductInput} from '@typings';
import React, {useEffect, useState} from 'react';
import OptionsWrapper, {OptionsSelectPayload} from '@components/OptionsWrapper';

interface AddProductProps {
  onSuccess: () => void;
}

const AddProduct: React.FC<AddProductProps> = ({onSuccess}) => {
  const {category, config} = useStore();
  const {translate} = useTranslate();

  const [validity, setValidity] = useState<ValidityState>(null);
  const [state, setState] = useState<ProductInput>(null);
  const [preview, setPreviewImage] = useState(null);
  const [image, setImage] = useState<string | ArrayBuffer>(null);
  const [isDisabled, disable] = useState(false);
  const [options, setOptions] = useState<OptionsSelectPayload>({});

  const {mutate, isSuccess} = createMutation<Product, ProductInput>(
    `addProduct`,
    addProduct,
  );

  const categories = []
    .concat(category.current?.parent && category.current)
    .concat(
      category.current?.children?.map((children) =>
        children.children?.length ? children.children : children,
      ),
    )
    .flat()
    .filter(Boolean);

  const onSubmit = async (e: React.ChangeEvent<HTMLFormElement>) => {
    e.preventDefault();
    await mutate(state);
  };

  const onChange = (
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
  ) => {
    setState({
      ...state,
      [e.target.name]:
        e.target.type === 'checkbox' ? !state?.featured : e.target.value,
    });

    setValidity(e.target.validity);
  };

  const onFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const reader = new FileReader();
    reader.readAsDataURL(e.target.files[0]);

    reader.onloadend = () => {
      setImage(reader.result);
      setPreviewImage(URL.createObjectURL(e.target.files[0]));
    };
  };

  useEffect(() => {
    setState({
      ...state,
      image,
    });
  }, [image]);

  useEffect(() => {
    setState({
      ...state,
      currency: config.currency?.id,
      options,
    });
  }, [config.currency, options]);

  useEffect(() => {
    if (!state) {
      return;
    }

    disable(
      !state.image ||
        !state.price ||
        !state.currency ||
        !state.name ||
        !state.category,
    );
  }, [state]);

  useEffect(() => {
    if (isSuccess) {
      onSuccess();
    }
  }, [isSuccess]);

  return (
    <form noValidate onSubmit={onSubmit} className="p-4 w-full">
      <label htmlFor="name">
        <p>{translate('placeholders.name')}</p>
        <input
          type="text"
          name="name"
          id="name"
          onChange={onChange}
          value={state?.name || ''}
        />
      </label>

      <div className="flex flex-row flex-wrap">
        <div className="w-8/12 mr-4">
          <label htmlFor="price">
            <p>{translate('placeholders.price')}</p>
            <input type="number" name="price" id="price" onChange={onChange} />
          </label>
        </div>

        <div className="flex-1">
          <label htmlFor="current" className="flex-1">
            <p>{translate('placeholders.currency')}</p>
            <div className="p-4 text-bold">{config.currency?.name}</div>
          </label>
        </div>
      </div>

      <label htmlFor="description">
        <p>{translate('placeholders.description')}</p>
        <textarea
          rows={5}
          name="description"
          id="description"
          value={state?.description}
          onChange={onChange}
        />
      </label>

      <label htmlFor="image" className="pb-4 border-b-2">
        <p>{translate('placeholders.image')}</p>
        {preview && (
          <div className="h-48 relative flex items-center justify-center mb-8">
            <img src={preview} alt={state?.name} className="h-full" />
          </div>
        )}
        <input
          type="file"
          name="image"
          id="image"
          accept="image/png, image/jpeg"
          capture
          onChange={onFileChange}
        />
      </label>

      <div className="flex flex-row flex-wrap border-b-2 pb-4">
        <label htmlFor="featured" className="flex-1 mr-4">
          <div className="flex flex-row flex-wrap items-center justify-center">
            <div className="w-2/12 flex items-center justify-center">
              <input
                type="checkbox"
                name="featured"
                id="featured"
                defaultChecked={!!state?.featured}
                onChange={onChange}
              />
            </div>
            <div className="flex-1">
              <p>{translate('placeholders.featured')}</p>
            </div>
          </div>
        </label>

        <label htmlFor="bestseller" className="flex-1">
          <div className="flex flex-row flex-wrap items-center justify-center">
            <div className="w-2/12 flex items-center justify-center">
              <input
                type="checkbox"
                name="bestseller"
                id="bestseller"
                defaultChecked={!!state?.bestseller}
                onChange={onChange}
              />
            </div>

            <div className="flex-1">
              <p>{translate('placeholders.bestseller')}</p>
            </div>
          </div>
        </label>
      </div>

      <label htmlFor="category" className="pb-4 border-b-2">
        <p>{translate('placeholders.category')}</p>
        <Select
          className="w-full"
          placeholder={translate('placeholders.category')}
          onChange={(select) =>
            setState({
              ...state,
              category: select,
            })
          }
          showValue
          value={state?.category || categories[0]?.id || category.current?.id}
          values={[
            ...new Set(
              categories?.map(({id, name}) => ({
                value: id,
                name,
              })),
            ),
          ]}
        />
      </label>

      {!!config.options?.length && (
        <OptionsWrapper data={config?.options} inline onSelect={setOptions} />
      )}

      <button
        type="submit"
        className="primary w-full"
        aria-label={translate('actions.add_product')}
        disabled={isDisabled || !validity?.valid}>
        {translate('actions.add_product')}
      </button>
    </form>
  );
};

export default AddProduct;
