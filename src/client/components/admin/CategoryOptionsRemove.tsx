import RestrictedArea from '@components/RestrictedArea';
import {createMutation, deleteCategoryOptionMutation} from '@queries';
import {Category, Option} from '@typings';
import React, {useEffect} from 'react';
import {hot} from 'react-hot-loader';

import {TiDelete} from 'react-icons/ti';

interface CategoryOptionsIndicatorProps {
  refetch: () => void;
  option: Option;
  category: Category;
}

const CategoryOptionsRemove: React.FC<CategoryOptionsIndicatorProps> = ({
  refetch,
  category,
  option,
}) => {
  const {mutate, isSuccess} = createMutation(
    'deleteCategoryOption',
    deleteCategoryOptionMutation,
  );

  useEffect(() => {
    if (isSuccess) {
      refetch();
    }
  }, [isSuccess]);

  return (
    <RestrictedArea scope="admin">
      <div className="ml-auto text-xl">
        <span
          className="hover:text-red-500 cursor:pointer transition ease-in-out cursor-pointer"
          role="presentation"
          onClick={() =>
            mutate({optionId: option.id, categoryId: category?.id})
          }>
          <TiDelete />
        </span>
      </div>
    </RestrictedArea>
  );
};

export default hot(module)(CategoryOptionsRemove);
