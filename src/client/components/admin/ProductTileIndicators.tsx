import RestrictedArea from '@components/RestrictedArea';
import {useTranslate} from '@providers';
import {createMutation, deleteProductMutation} from '@queries';
import {Product} from '@typings';
import React, {useEffect} from 'react';
import {hot} from 'react-hot-loader';

interface AdminProductTileIndicatorsProps {
  product: Product;
  onDelete: () => void;
}
const AdminProductTileIndicators: React.FC<AdminProductTileIndicatorsProps> = ({
  product,
  onDelete,
}) => {
  const {mutate, isSuccess} = createMutation(
    'deleteProduct',
    deleteProductMutation,
  );
  const {translate} = useTranslate();

  useEffect(() => {
    if (isSuccess) {
      onDelete();
    }
  }, [isSuccess]);
  return (
    <RestrictedArea scope="admin">
      <div className="absolute top-0 right-0 text-gray-900 dark:bg-gray-900 dark:text-white">
        <div className="relative flex flex-row flex-wrap ml-auto">
          <button
            type="button"
            className="text-xs text-red-500"
            onClick={() => mutate(product.id)}>
            {translate('actions.delete')}
          </button>
        </div>
      </div>
    </RestrictedArea>
  );
};

export default hot(module)(AdminProductTileIndicators);
