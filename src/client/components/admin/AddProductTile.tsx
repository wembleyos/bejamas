import React, {useState} from 'react';
import {useTranslate} from '@providers';
import Loadable from '@loadable/component';

import Modal from '@components/Modal';
import RestrictedArea from '@components/RestrictedArea';

const AddProduct = Loadable(() => import('./forms/AddProduct'));

interface AddProductTileProps {
  onSuccess: () => void;
}

const AddProductTile: React.FC<AddProductTileProps> = ({onSuccess}) => {
  const {translate} = useTranslate();
  const [isModalOpened, toggleModal] = useState(false);

  const handleSuccess = () => {
    onSuccess();
    toggleModal(false);
  };
  return (
    <RestrictedArea scope="admin">
      <>
        {isModalOpened && (
          <Modal isOpen={isModalOpened} onClose={() => toggleModal(false)}>
            <AddProduct onSuccess={handleSuccess} />
          </Modal>
        )}
        <div className="product-tile">
          <div
            className="image-wrapper flex items-center cursor-pointer"
            role="presentation"
            onClick={() => toggleModal(true)}>
            {translate('actions.add_product')}
          </div>
        </div>
      </>
    </RestrictedArea>
  );
};

export default AddProductTile;
