import Modal from '@components/Modal';
import RestrictedArea from '@components/RestrictedArea';
import {useTranslate} from '@providers';
import {
  createMutation,
  createCategoryMutation,
  createQuery,
  getConfig,
} from '@queries';
import {useStore} from '@store';
import React, {useState, useEffect} from 'react';
import {hot} from 'react-hot-loader';

interface NewCategoryModalProps {
  refetch: () => void;
  toggle: () => void;
  isOpened: boolean;
}

const NewCategoryModal: React.FC<NewCategoryModalProps> = ({
  isOpened,
  toggle,
  refetch,
}) => {
  const {category, config} = useStore();
  const {translate} = useTranslate();
  const [name, setCategoryName] = useState('');
  const [options, setOptions] = useState<string[]>([]);
  const [isRoot, setRoot] = useState(false);
  const {refetch: configRefetch} = createQuery('config', getConfig);
  const {mutate, isSuccess} = createMutation(
    'createCategory',
    createCategoryMutation,
  );

  const handleClose = () => {
    toggle();
  };

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setCategoryName(e.target.value);
  };

  const handleSubmit = (e: React.ChangeEvent<HTMLFormElement>) => {
    e.preventDefault();
    mutate({parent: category.current?.id, name, options, isRoot});
  };

  const handleOptionsChange = (option: string) => {
    setOptions(
      options.includes(option)
        ? options.filter((item) => item !== option)
        : options.concat(option),
    );
  };
  useEffect(() => {
    if (isSuccess) {
      refetch();
      configRefetch();
      toggle();
    }
  }, [isSuccess]);

  return (
    <RestrictedArea scope="admin">
      <Modal onClose={() => handleClose()} isOpen={isOpened}>
        <form
          className="p-4 px-8 flex flex-col flex-wrap"
          onSubmit={handleSubmit}>
          <h3 className="text-heading mb-4">
            {translate('headers.new_category')}
          </h3>

          <div className="flex flex-row flex-wrap items-center">
            <label htmlFor="newCategory" className="mb-8 flex-1">
              <p>{translate('placeholders.category')}</p>
              <input
                type="text"
                name="category"
                id="newCategory"
                placeholder={translate('placeholders.category')}
                value={name}
                onChange={handleChange}
              />
            </label>

            <label
              htmlFor="isRoot"
              className="ml-4 w-2/12 flex flex-col flex-wrap items-center justify-center">
              <p>{translate('placeholders.is_root_category')}</p>
              <input
                type="checkbox"
                defaultChecked={isRoot}
                name="isRoot"
                id="isRoot"
                className="mr-4"
                onClick={() => setRoot(!isRoot)}
              />
            </label>
          </div>

          {!!config.options?.length && (
            <div className="mb-8">
              <p className="mb-4 font-semibold">
                {translate('placeholders.options')}
              </p>
              <div className="grid grid-cols-3 gap-4 mb-8">
                {config.options.map((option) => (
                  <div key={option.id} className="flex flex-row flex-wrap">
                    <input
                      type="checkbox"
                      defaultChecked={options.includes(option.id)}
                      name={option.value}
                      className="mr-4"
                      onClick={() => handleOptionsChange(option.id)}
                    />
                    <p className="text-label">{option.name}</p>
                  </div>
                ))}
              </div>
            </div>
          )}

          <button className="primary w-full" type="submit">
            {translate('actions.create_new')}
          </button>
        </form>
      </Modal>
    </RestrictedArea>
  );
};

interface NewCategoryWrapperProps {
  refetch: () => void;
}

const NewCategoryWrapper: React.FC<NewCategoryWrapperProps> = ({refetch}) => {
  const {translate} = useTranslate();
  const [isNewCategoryModal, toggleNewCategoryModal] = useState(false);

  return (
    <div className="flex flex-col flex-wrap mb-8">
      {isNewCategoryModal && (
        <NewCategoryModal
          isOpened={isNewCategoryModal}
          refetch={refetch}
          toggle={() => toggleNewCategoryModal(!isNewCategoryModal)}
        />
      )}
      <button
        className="primary ml-auto"
        type="button"
        onClick={() => toggleNewCategoryModal(true)}>
        {translate('actions.add_new')}
      </button>
    </div>
  );
};

export default hot(module)(NewCategoryWrapper);
