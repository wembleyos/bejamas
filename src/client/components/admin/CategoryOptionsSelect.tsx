import Modal from '@components/Modal';
import RestrictedArea from '@components/RestrictedArea';
import {useTranslate} from '@providers';
import {createMutation, updateCategoryOptionsMutation} from '@queries';
import {useStore} from '@store';
import {Option} from '@typings';
import React, {useEffect, useState} from 'react';
import {hot} from 'react-hot-loader';

interface CategoryOptionsSelectModalProps {
  isOpen: boolean;
  onClose: () => void;
}

const CategoryOptionsSelectModal: React.FC<CategoryOptionsSelectModalProps> = ({
  isOpen,
  onClose,
}) => {
  const {translate} = useTranslate();
  const {config, category} = useStore();
  const {isSuccess, mutate} = createMutation(
    'updateCategoryOptions',
    updateCategoryOptionsMutation,
  );
  const [selected, setSelected] = useState<string[]>(
    category.current?.options?.map((option) => option.id) || [],
  );

  const handleSelect = (optionId: string) => {
    setSelected(
      selected.includes(optionId)
        ? selected.filter((option) => option !== optionId)
        : selected.concat(optionId),
    );
  };

  const handleSubmit = async () => {
    await mutate({options: selected, categoryId: category.current?.id});
  };

  useEffect(() => {
    if (isSuccess) {
      onClose();
    }
  }, [isSuccess]);

  return (
    <Modal isOpen={isOpen} onClose={onClose}>
      <div className="p-4">
        <h3 className="text-heading mb-8">
          {translate('headers.chose_category_options')}
        </h3>

        <div className="">
          <div className="grid grid-cols-3 gap-4 mb-8">
            {config.options.map((option) => (
              <div key={option.id} className="flex flex-row flex-wrap">
                <input
                  type="checkbox"
                  defaultChecked={selected.includes(option.id)}
                  className="mr-4"
                  onClick={() => handleSelect(option.id)}
                />
                <p className="text-label">{option.name}</p>
              </div>
            ))}
          </div>
        </div>
        <button
          type="submit"
          className="primary w-full"
          onClick={() => handleSubmit()}>
          {translate('actions.chose_options')}
        </button>
      </div>
    </Modal>
  );
};

interface CategoryOptionsSelectProps {
  refetch: () => void;
  selected: Option[];
}

const CategoryOptionsSelect: React.FC<CategoryOptionsSelectProps> = ({
  refetch,
}) => {
  const {translate} = useTranslate();
  const [isModalOpened, setModalState] = useState(false);

  const handleClose = () => {
    setModalState(false);
    return refetch();
  };

  useEffect(() => {}, []);
  return (
    <RestrictedArea scope="admin">
      <>
        {isModalOpened && (
          <CategoryOptionsSelectModal
            isOpen={isModalOpened}
            onClose={handleClose}
          />
        )}
        <div className="mb-8">
          <button
            type="button"
            className="primary w-full"
            onClick={() => setModalState(true)}>
            {translate('actions.chose_options')}
          </button>
        </div>
      </>
    </RestrictedArea>
  );
};

export default hot(module)(CategoryOptionsSelect);
