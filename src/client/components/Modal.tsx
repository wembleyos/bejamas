import React, {useEffect} from 'react';
import {Helmet} from 'react-helmet';
import CloseIcon from '../assets/CloseIcon';

interface ModalProps {
  children: React.ReactElement;
  onClose: () => void;
  isOpen: boolean;
}

const Modal: React.FC<ModalProps> = ({children, onClose}) => {
  if (typeof window === 'undefined') {
    return null;
  }
  useEffect(
    () => () => {
      onClose();
    },
    [],
  );

  return (
    <>
      <Helmet>
        <body className="overflow-hidden" />
      </Helmet>
      <div className="modal" role="presentation" onClick={() => onClose()}>
        <div
          className="modal-content"
          role="presentation"
          onClick={(e) => e.stopPropagation()}>
          <div className="flex flex-1 items-center justify-end p-2 absolute right-2 top-2 z-20 cursor-pointer dark:text-white">
            <CloseIcon onClick={() => onClose && onClose()} />
          </div>
          {children}
        </div>
      </div>
    </>
  );
};

export default Modal;
