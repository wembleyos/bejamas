import React from 'react';
import {hot} from 'react-hot-loader';

import {renderRoutes} from 'react-router-config';
import routes from '../routes';

const App: React.FC = () => renderRoutes(routes);

export default hot(module)(App);
