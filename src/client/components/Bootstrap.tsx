import React, {useEffect} from 'react';
import {Helmet} from 'react-helmet';
import {useStore} from '@store';
import {createQuery, getConfig} from '@queries';
import Providers from '@providers';
import {setConfig} from '@actions';
import Layout from './layout';

interface BootstrapProps {
  children: React.ReactElement;
}

const {DOMAIN, APP_NAME} = process.env;

const Bootstrap: React.FC<BootstrapProps> = ({children}) => {
  const {data} = createQuery('config', getConfig);
  const {dispatch} = useStore();
  const isDark =
    typeof window !== 'undefined' &&
    window.matchMedia('(prefers-color-scheme: dark)')?.matches;

  useEffect(() => {
    dispatch(setConfig(data));
  }, [data]);

  useEffect(() => {
    if (typeof window !== 'undefined') {
      /* eslint-disable */
      if (typeof (window as any).__REACT_DEVTOOLS_GLOBAL_HOOK__ === 'object') {
        (window as any).__REACT_DEVTOOLS_GLOBAL_HOOK__.inject = function () {};
      }
      /* eslint-enable */
    }
  }, []);

  return (
    <Providers config={data}>
      <>
        <Helmet
          titleTemplate={`${DOMAIN || APP_NAME} - %s`}
          defaultTitle={APP_NAME || DOMAIN}>
          <body className="dark:bg-black dark:text-white text-black" />
          <meta name="theme-color" content={isDark ? '#000' : '#FFF'} />
        </Helmet>
        <Layout>{children}</Layout>
      </>
    </Providers>
  );
};

export default Bootstrap;
