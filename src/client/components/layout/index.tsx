import React from 'react';
import Header from './Header';

interface LayoutProps {
  children: React.ReactElement;
}

const Layout: React.FC<LayoutProps> = ({children}) => (
  <>
    <div className="fixed top-0 left-0 w-full flex bg-white dark:bg-black z-10">
      <div className="container relative h-auto flex items-center">
        <Header />
      </div>
    </div>
    <main className="container viewbox">{children}</main>
  </>
);

export default Layout;
