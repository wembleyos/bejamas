import {toggleMinicart} from '@actions';
import Minicart from '@components/Minicart';
import {useStore} from '@store';
import React from 'react';
import {Link} from 'react-router-dom';
import Logo from '../../assets/Logo';
import IconCart from '../../assets/IconCart';
import HeaderCategories from './HeaderCategories';

const Header: React.FC = () => {
  const {dispatch, config} = useStore();

  return (
    <header className="flex items-center justify-between w-full border-b border-b-2 dark:border-gray-900 py-4 relative bg-white dark:bg-black">
      <Link to="/" className="mr-4">
        <Logo />
      </Link>
      <div className="mr-auto">
        <HeaderCategories categories={config.categories} />
      </div>
      <div className="flex flex-row flex-wrap items-center">
        <div className="hover:opacity-50 transition ease-in-out">
          <IconCart
            onClick={() => dispatch(toggleMinicart())}
            className="cursor-pointer"
          />
        </div>
      </div>
      <Minicart />
    </header>
  );
};

export default Header;
