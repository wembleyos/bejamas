import {Category} from '@typings';
import React, {useEffect} from 'react';
import {Link} from 'react-router-dom';

interface HeaderCategoriesProps {
  categories: Category[];
}
const HeaderCategories: React.FC<HeaderCategoriesProps> = ({categories}) => {
  useEffect(() => {}, []);
  return (
    <ul className="flex flex-row flex-wrap">
      {categories.map((item) => (
        <li key={item.id}>
          <Link
            to={`/catalog/${item.slug.trim()}`}
            className="mr-2 last:mr-2 p-2 px-4 font-bold uppercase">
            {item.name}
          </Link>
        </li>
      ))}
    </ul>
  );
};

export default HeaderCategories;
