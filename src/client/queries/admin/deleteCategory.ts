import {DeleteCategoryArgs} from '@typings';
import request from '../../request';

const deleteCategoryMutation = async (input: DeleteCategoryArgs) => {
  const {data} = await request.delete(`/category/${input.categoryId}`);
  return data;
};
export default deleteCategoryMutation;
