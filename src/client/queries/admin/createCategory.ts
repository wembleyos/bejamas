import {NewCategoryArgs} from '@typings';
import request from '../../request';

const createCategoryMutation = async (input: NewCategoryArgs) => {
  const {data} = await request.post(`/category`, input);
  return data;
};
export default createCategoryMutation;
