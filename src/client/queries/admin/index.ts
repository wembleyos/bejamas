import deleteProductMutation from './deleteProduct';
import createCategoryMutation from './createCategory';
import deleteCategoryMutation from './deleteCategory';
import deleteCategoryOptionMutation from './deleteCategoryOption';
import updateCategoryOptionsMutation from './updateCategoryOptions';

export {
  deleteProductMutation,
  createCategoryMutation,
  deleteCategoryMutation,
  deleteCategoryOptionMutation,
  updateCategoryOptionsMutation,
};

export default {
  deleteProductMutation,
  createCategoryMutation,
  deleteCategoryMutation,
  deleteCategoryOptionMutation,
  updateCategoryOptionsMutation,
};
