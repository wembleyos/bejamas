import {UpdateCategoryOptionsArgs} from '@typings';
import request from '../../request';

const updateCategoryOptionsMutation = async ({
  categoryId,
  options,
}: UpdateCategoryOptionsArgs) => {
  const {data} = await request.patch(`/category/options/`, {
    options,
    categoryId,
  });
  return data;
};
export default updateCategoryOptionsMutation;
