import {DeleteCategoryOptionArgs} from '@typings';
import request from '../../request';

const deleteCategoryOptionMutation = async ({
  categoryId,
  optionId,
}: DeleteCategoryOptionArgs) => {
  const {data} = await request.delete(
    `/category/option/${categoryId}/${optionId}`,
  );
  return data;
};
export default deleteCategoryOptionMutation;
