import request from '../../request';

const deleteProductMutation = async (productId: string) => {
  const {data} = await request.delete(`/product/${productId}`);
  return data;
};
export default deleteProductMutation;
