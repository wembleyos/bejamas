import {
  MutationFunction,
  MutationOptions,
  QueryFunction,
  useMutation,
  useQuery,
  UseQueryOptions,
} from 'react-query';

import {AxiosError} from 'axios';
import {HttpError} from 'http-errors';

export function createQuery<Payload>(
  queryKey: string,
  queryFn: QueryFunction<Payload>,
  options?: UseQueryOptions<Payload, AxiosError<HttpError>>,
) {
  return useQuery<Payload, AxiosError<HttpError>>(
    queryKey,
    queryFn,
    options || {},
  );
}

export function createMutation<Payload, Variables>(
  mutationKey: string,
  mutationFn: MutationFunction<Payload, Variables>,
  options?: MutationOptions<Payload, AxiosError<HttpError>, Variables>,
) {
  return useMutation<Payload, AxiosError<HttpError>, Variables>(
    mutationKey,
    mutationFn,
    options || {},
  );
}

export * from './_queries';
