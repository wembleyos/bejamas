import {Address, Product} from '@typings';
import request from '../../request';

export interface PlaceOrderMutationArgs {
  billingAddress: Address;
  shippingAddress: Address;
  products: Product[];
}

const placeOrderMutation = async (payload: PlaceOrderMutationArgs) => {
  const {data} = await request.post('/order', payload);

  return data;
};

export default placeOrderMutation;
