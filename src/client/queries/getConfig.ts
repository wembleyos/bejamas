import {ConfigState} from '@typings';
import request from '../request';

const getConfig = async () => {
  const {data} = await request.get<ConfigState>('/config');

  return data;
};

export default getConfig;
