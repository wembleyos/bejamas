import {Category, CategoryMatch, CategorySearchParams} from '@typings';
import request from '../../request';

const getCategory =
  (match?: CategoryMatch, params?: CategorySearchParams) => async () => {
    try {
      const {data} = await request.get<Category>(
        `/category/${Object.values(match).join('/') || ''}`.replace(
          /([^:]\/)\/+/g,
          '$1',
        ),
        {
          params,
        },
      );
      return data;
    } catch (e) {
      throw new Error(e.message);
    }
  };

export default getCategory;
