import request from '../../request';

export interface ChangePotdArgs {
  product: string;
  category: string;
}

const changePotd = async (payload: ChangePotdArgs) => {
  const {data} = await request.post('/product/potd', payload);

  return data;
};

export default changePotd;
