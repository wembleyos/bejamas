import changePotd from './changePotd';
import getCategory from './getCategory';
import getFeatured from './getFeatured';

export {changePotd, getCategory, getFeatured};

export default {
  changePotd,
  getCategory,
  getFeatured,
};
