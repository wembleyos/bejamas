import {Product} from '@typings';
import request from '../../request';

const getFeatured = (category?: string) => async () => {
  try {
    if (!category) {
      return [];
    }
    const {data} = await request.get<Product[]>(
      `/category/featured/${category}`.replace(/([^:]\/)\/+/g, '$1'),
    );
    return data;
  } catch (e) {
    throw new Error(e.message);
  }
};

export default getFeatured;
