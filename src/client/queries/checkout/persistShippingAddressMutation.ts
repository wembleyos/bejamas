import {Address} from '@typings';
import request from '../../request';

export interface PersistBillingAddressMutationhangePotdArgs {
  address: Address;
  type: 'shipping';
}

const persistShippingAddressMutation = async (
  payload: PersistBillingAddressMutationhangePotdArgs,
) => {
  const {data} = await request.patch('/checkout', payload);

  return data;
};

export default persistShippingAddressMutation;
