import {Address} from '@typings';
import request from '../../request';

export interface PersistBillingAddressMutationhangePotdArgs {
  address: Address;
  type: 'billing';
}

const persistBillingAddressMutation = async (
  payload: PersistBillingAddressMutationhangePotdArgs,
) => {
  const {data} = await request.patch('/checkout', payload);

  return data;
};

export default persistBillingAddressMutation;
