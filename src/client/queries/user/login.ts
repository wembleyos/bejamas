import {Credentials, User} from '@typings';
import request from '../../request';

const login = async (credentials: Credentials) => {
  const {data} = await request.post<User>('/login', credentials);
  return data;
};
export default login;
