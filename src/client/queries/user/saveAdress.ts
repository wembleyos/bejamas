import {Address} from '@typings';
import request from '../../request';

const saveAddressMutation = async (address: Address) => {
  const {data} = await request.post<Address>('/user/address', address);

  return data;
};

export default saveAddressMutation;
