import {Address} from '@typings';
import request from '../../request';

const getActiveAdresssQuery = async () => {
  const {data} = await request.get<Address>('/user/address');
  return data;
};

export default getActiveAdresssQuery;
