import {User} from '@typings';
import request from '../../request';

const currentUser = async () => {
  const {data} = await request.get<User>('/user');
  return data;
};

export default currentUser;
