import currentUser from './currentUser';
import login from './login';
import register from './register';

import saveAddress from './saveAdress';
import getActiveAdresssQuery from './getActiveAdress';

export {currentUser, login, register, saveAddress, getActiveAdresssQuery};

export default {
  currentUser,
  login,
  register,
  saveAddress,
  getActiveAdresssQuery,
};
