import {RegisterData} from '@typings';
import request from '../../request';

const register = async (registerData: RegisterData) => {
  const {data} = await request.post('/register', registerData);

  return data;
};

export default register;
