import {ProductInput} from '@typings';
import request from '../request';

const addProduct = async (productInput: ProductInput) => {
  const {data} = await request.post('/product', productInput);
  return data;
};

export default addProduct;
