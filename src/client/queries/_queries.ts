import getConfig from './getConfig';
import addProduct from './addProduct';

export * from './admin';
export * from './category';
export * from './user';
export * from './order';

export {addProduct, getConfig};

export default {
  addProduct,
  getConfig,
};
