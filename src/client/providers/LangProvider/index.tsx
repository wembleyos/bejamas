import {ConfigState} from '@typings';
import {get} from 'lodash';
import React, {createContext, useContext, useEffect, useState} from 'react';
import translations from './translations';

interface LangState {
  translate: (key: string) => string;
}

const Context = createContext<LangState>({
  translate: () => null,
});

interface LangProviderProps {
  children: React.ReactElement;
  config: ConfigState;
}

const LangProvider: React.FC<LangProviderProps> = ({children, config}) => {
  const language = 'en';
  const [translationKeys, setTranslations] = useState(
    get(translations, language),
  );

  useEffect(() => {
    setTranslations(get(translations, [language]));
  }, [config]);

  const translate = (key: string) => get(translationKeys, key) || key;

  return <Context.Provider value={{translate}}>{children}</Context.Provider>;
};

export const useTranslate = () => useContext(Context);

export default LangProvider;
