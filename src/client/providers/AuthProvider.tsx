import {setAuthenticate} from '@actions';
import {createQuery, currentUser} from '@queries';
import {useStore} from '@store';
import {User, userState} from '@typings';
import React, {useEffect, useContext} from 'react';
import {useHistory, useLocation} from 'react-router';

interface AuthProviderProps {
  children: React.ReactElement;
}

interface AuthState {
  hasRole: (roleName: string) => boolean;
  user: typeof userState;
}

const Context = React.createContext<AuthState>(null);

const AuthProvider: React.FC<AuthProviderProps> = ({children}) => {
  const {dispatch, user} = useStore();
  const location = useLocation();
  const history = useHistory();

  const {data, error} = createQuery<User>('currentUser', currentUser);

  useEffect(() => {
    if (dispatch) {
      dispatch(setAuthenticate(data));
    }
  }, [data]);

  useEffect(() => {
    if (error) {
      dispatch(setAuthenticate(null));
    }
  }, [error]);

  useEffect(() => {
    if (!data?.id) {
      return;
    }

    if (location.pathname === '/login') {
      history.push('/');
    }

    if (location.pathname === '/register') {
      history.push('/');
    }
  }, [location.pathname, data]);

  const hasRole: AuthState['hasRole'] = (roleName: string) =>
    user &&
    user.data?.role
      ?.filter(Boolean)
      ?.some((role) => role.name?.toLowerCase() === roleName.toLowerCase());

  const methods = {
    hasRole,
  };

  return (
    <Context.Provider value={{...methods, user}}>{children}</Context.Provider>
  );
};

export const useAuth = () => useContext(Context);

export default AuthProvider;
