import {ConfigState} from '@typings';
import React from 'react';

import AuthProvider from './AuthProvider';
import LangProvider from './LangProvider';

interface ProviderProps {
  children: React.ReactElement;
  config: ConfigState;
}

const Providers: React.FC<ProviderProps> = ({children, config}) => (
  <AuthProvider>
    <LangProvider config={config}>{children}</LangProvider>
  </AuthProvider>
);

export {useTranslate} from './LangProvider';

export default Providers;
