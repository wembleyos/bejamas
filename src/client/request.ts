import axios from 'axios';

const {DOMAIN, PORT} = process.env;

const request = axios.create({
  baseURL: (DOMAIN?.length && DOMAIN) || `http://localhost:${PORT}/api`,
});

request.interceptors.response.use(
  (response) => response,
  (error) => Promise.reject(error),
);

request.interceptors.request.use(
  (response) => response,
  (error) => Promise.reject(error),
);

export default request;
