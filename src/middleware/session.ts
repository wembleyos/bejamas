import expressSession from 'express-session';
import dotenv from 'dotenv';
import redis from 'redis';

import connectRedis from 'connect-redis';
import UserEntity from '@entities/UserEntity';

dotenv.config();

export type Scope =
  | 'write:products'
  | 'write:options'
  | 'write:categories'
  | 'write:orders';

declare module 'express-session' {
  interface SessionData {
    user: UserEntity;
    scopes: Scope[];
  }
}

export const RedisStore = connectRedis(expressSession);

export const redisClient = redis.createClient({
  host: process.env.REDIS_HOST || 'localhost',
  port: Number(process.env.REDIS_HOST || '6379'),
  password: process.env.REDIS_PASSWORD,
});

const session = () =>
  expressSession({
    secret: process.env.SECRET,
    resave: false,
    saveUninitialized: false,
    cookie: {secure: false},
    store: new RedisStore({client: redisClient}),
  });

export default session;
