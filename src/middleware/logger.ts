import chalk from 'chalk';
import winston, {format} from 'winston';

const {combine} = format;
const container = new winston.Container();

const myFormat = format.printf(({level, message, label, timestamp}) => {
  const result = [
    timestamp && chalk.gray(new Date(timestamp).toLocaleString('pl-PL')),
    label && chalk.bold(` [${label}]`.toUpperCase()),
    level && ` ${level}`,
    message && `: ${message}`,
  ];

  return result.filter(Boolean).join('');
});

const logger = winston.createLogger({
  format: combine(
    format.timestamp(),
    format.colorize(),
    format.json(),
    format.splat(),
    format.simple(),
    myFormat,
  ),
  transports: [new winston.transports.Console()],
  level: 'debug',
});

container.add('product', {
  format: combine(
    format.timestamp(),
    format.label({label: 'product'}),
    format.colorize(),
    format.json(),
    format.splat(),
    format.simple(),
    myFormat,
  ),
  transports: [new winston.transports.Console()],
  level: 'debug',
});

container.add('validation', {
  format: combine(
    format.timestamp(),
    format.label({label: 'validation'}),
    format.colorize(),
    format.json(),
    format.splat(),
    format.simple(),
    myFormat,
  ),
  transports: [new winston.transports.Console()],
  level: 'debug',
});

container.add('category', {
  format: combine(
    format.timestamp(),
    format.label({label: 'category'}),
    format.colorize(),
    format.json(),
    format.splat(),
    format.simple(),
    myFormat,
  ),
  transports: [new winston.transports.Console()],
  level: 'debug',
});

container.add('auth', {
  format: combine(
    format.timestamp(),
    format.label({label: 'auth'}),
    format.colorize(),
    format.json(),
    format.splat(),
    format.simple(),
    myFormat,
  ),
  transports: [new winston.transports.Console()],
  level: 'debug',
});

container.add('auth', {
  format: combine(
    format.timestamp(),
    format.label({label: 'auth'}),
    format.colorize(),
    format.json(),
    format.splat(),
    format.simple(),
    myFormat,
  ),
  transports: [new winston.transports.Console()],
  level: 'debug',
});

export {container};

export default logger;
