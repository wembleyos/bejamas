import React from 'react';
import {Request, Response} from 'express';
import {renderToString} from 'react-dom/server';
import {StaticRouter} from 'react-router';
import routes from 'src/client/routes';
import Helmet from 'react-helmet';
import {matchRoutes, renderRoutes} from 'react-router-config';

import {QueryClient, QueryClientProvider} from 'react-query';
import {dehydrate, Hydrate} from 'react-query/hydration';
import {Context} from '@typings';
import path from 'path';
import {getConfig} from '@queries';
import logger from './logger';

const loadBranchData = (
  location: string,
  queryClient?: QueryClient,
  context?: Context,
) => {
  const branch = matchRoutes(routes, location);

  const promises = branch.map(({route, match}) =>
    route.loadData
      ? route.loadData(match, queryClient, context)
      : Promise.resolve(null),
  );

  return Promise.all(promises);
};

const htmlRenderer = async (req: Request, res: Response) => {
  const {devMiddleware} = res.locals.webpack;
  const {outputFileSystem} = devMiddleware;
  const jsonWebpackStats = devMiddleware.stats.toJson();
  const {assetsByChunkName, outputPath} = jsonWebpackStats;
  const assets = Object.values(assetsByChunkName)
    .flat()
    .filter(Boolean) as string[];
  const context: Context = {};

  const queryClient = new QueryClient();
  let dehydratedState;

  try {
    res.cookie('XSRF-TOKEN', req.csrfToken());

    await queryClient.prefetchQuery('config', getConfig);
    await loadBranchData(req.originalUrl, queryClient, context);
    dehydratedState = dehydrate(queryClient);
  } catch (e) {
    logger.error('Error during loading SSR Data: %s', e.message);
  }

  let jsx = '';
  try {
    jsx = renderToString(
      <QueryClientProvider client={queryClient}>
        <Hydrate state={dehydratedState}>
          <StaticRouter context={context} location={req.path}>
            {renderRoutes(routes)}
          </StaticRouter>
        </Hydrate>
      </QueryClientProvider>,
    );
  } catch (e) {
    logger.error('Error during SSR: %s', e.message);
    context.statusCode = 500;
    jsx = '';
  }

  const helmet = Helmet.renderStatic();

  return res.status(context.statusCode || 200).send(`<!DOCTYPE html>
    <html  lang="${req.acceptsLanguages('en', 'pl')}" ${
    helmet.htmlAttributes.toString() || ''
  }>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="manifest" href="/manifest.json" crossorigin="use-credentials">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <base href="/" />
        ${helmet.title.toString()}
        ${helmet.meta.toString()}
        ${helmet.link.toString()}
        ${assets
          .filter((file) => file.endsWith('.css'))
          .map((file) =>
            process.env.NODE_ENV === 'development'
              ? `<link rel="stylesheet" href="/${file}" media="all" />`
              : `<style>${outputFileSystem.readFileSync(
                  path.join(outputPath, file),
                )}</style>`.trim(),
          )
          .join()}
    </head>
    <body ${helmet.bodyAttributes.toString() || ''}>
    <div id="app">${jsx}</div>
    <div id="modal"></div>
    ${assets
      .filter((file: string) => file.endsWith('.js'))
      .filter((file: string) => !file.includes('hot'))
      .map((file: string) => `<script src="/${file}" defer></script>`)
      .filter(Boolean)
      .join()}
    <script>
    window.__REACT_QUERY_STATE__ = ${JSON.stringify(dehydratedState)}
    </script>
    </body>
    </html>`);
};

export default htmlRenderer;
