import webpack from 'webpack';
import WebpackDevMiddleware from 'webpack-dev-middleware';
import WebpackHotMiddleware from 'webpack-hot-middleware';

import dotenv from 'dotenv';
import webpackConfig from '../../config/webpack.config';

dotenv.config();

const compiler = webpack(webpackConfig);

export const devMiddleware = WebpackDevMiddleware(compiler, {
  serverSideRender: true,
  stats: process.env.NODE_ENV === 'development' ? false : 'errors-only',
  writeToDisk: process.env.NODE_ENV !== 'development',
});

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const hotMiddleware = WebpackHotMiddleware(compiler as any, {
  log: false,
});

export default {
  devMiddleware,
  hotMiddleware,
};
