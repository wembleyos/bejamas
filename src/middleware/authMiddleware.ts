import UserEntity from '@entities/UserEntity';
import {NextFunction, Request, Response} from 'express';
import createHttpError from 'http-errors';

import {Scope} from '@middleware/session';
import {getRepository} from 'typeorm';
import logger from './logger';

const authMiddleware = (req: Request, res: Response, next: NextFunction) => {
  if (req.session.user) {
    return next();
  }
  return res.status(401).end('unauthorized');
};

export const claimRole =
  (roles: string[]) =>
  async (req: Request, res: Response, next: NextFunction) => {
    if (!req.session.user) {
      return next(createHttpError(401));
    }

    const {role} = await UserEntity.findOne(req.session.user.id, {
      relations: ['role'],
    });

    if (role?.some(({name}) => roles.includes(name))) {
      return next();
    }

    logger.warning(
      'Access denied for %s at %s',
      req.session.user.email,
      req.originalUrl,
    );
    return next(createHttpError(403));
  };

export const claimScope =
  (scopesToClaim: Scope[]) =>
  async (req: Request, res: Response, next: NextFunction) => {
    if (!req.session.user) {
      return next(createHttpError(401));
    }

    const user = await getRepository(UserEntity)
      .createQueryBuilder('user')
      .leftJoin('user.role', 'role')
      .addSelect('role.scope', 'scope')
      .select(['scope', 'user', 'role.scope'])
      .where('user.id = :userId', {userId: req.session.user.id})
      .getOne();

    if (!user) {
      return next(createHttpError(403));
    }

    if (!user.role?.length) {
      logger.warning(
        'Access denied for %s at %s',
        req.session.user.email,
        req.originalUrl,
      );
      return next(createHttpError(403));
    }

    const {role} = user;

    const scopes = role
      .map(({scope}) => scope)
      .flat()
      .map((item) => item.split(','))
      .flat() as Scope[];

    req.session.scopes = scopes;
    req.session.save();

    if (
      scopesToClaim.some((scopeToClaim) =>
        scopes.includes(scopeToClaim as Scope),
      )
    ) {
      return next();
    }

    logger.warning(
      'Access denied for %s at %s',
      req.session.user.email,
      req.originalUrl,
    );
    return next(createHttpError(403));
  };
export default authMiddleware;
