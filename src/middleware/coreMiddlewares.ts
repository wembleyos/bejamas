import bodyParser from 'body-parser';
import responseTime from 'response-time';

import express from 'express';
import compression from 'compression';
import csurf from 'csurf';
import session from './session';
import helmet from './helmet';

const coreMiddlewares: express.RequestHandler[] = [
  responseTime(),
  compression({level: 9}),
  bodyParser.urlencoded({extended: false, limit: '1mb'}),
  bodyParser.json({limit: '1mb'}),
  session(),
  ...helmet,
  csurf(),
];

export default coreMiddlewares;
