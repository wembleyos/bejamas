import {NextFunction, Request, Response} from 'express';
import Joi from 'joi';
import createError from 'http-errors';
import {container} from '@middleware/logger';

const logger = container.get('validation');

const validate =
  (schema: Joi.PartialSchemaMap<unknown>, property?: 'params' | 'query') =>
  (req: Request, res: Response, next: NextFunction) => {
    const object = Joi.object(schema);
    const {error} = object.validate(req[property] || req.body, schema);
    const valid = error == null;

    if (valid) {
      return next();
    }

    const {details} = error;
    const message = details.map(({path, ...rest}, i) => ({
      [path[i]]: rest.message.replace(/"([^"]+(?="))"/g, '$1'),
    }));
    message.forEach((item) => {
      logger.error(Object.values(item)[0]);
    });
    res.status(422);
    return res.json(
      createError(422, {
        validation: details.map(({path, type}, i) => ({
          [path[i]]: type,
        })),
      }),
    );
  };

export default validate;
