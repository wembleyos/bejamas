import express from 'express';

import orderCreate from './Order.Create';

const order = express.Router();

order.post('/', orderCreate);

export default order;
