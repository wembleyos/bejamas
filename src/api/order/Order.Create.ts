import AddressEntity from '@entities/AddressEntity';
import {PlaceOrderArgs} from '@typings';
import {Request, Response} from 'express';

const orderCreate = async (
  req: Request<unknown, unknown, PlaceOrderArgs>,
  res: Response,
) => {
  const shippingAddress =
    (await AddressEntity.findOne(req.body?.shippingAddress?.id)) ||
    req.body.shippingAddress;

  const billingAddress =
    (await AddressEntity.findOne(req.body?.billingAddress?.id)) ||
    req.body.billingAddress;

  res.json({shippingAddress, billingAddress});
};

export default orderCreate;
