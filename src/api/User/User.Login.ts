import UserEntity from '@entities/UserEntity';
import {Credentials} from '@typings';
import {Request, Response} from 'express';
import dotenv from 'dotenv';

import bcrypt from 'bcryptjs';
import createHttpError from 'http-errors';

import {createQueryBuilder} from 'typeorm';
import logger from '@middleware/logger';

dotenv.config();

const userLogin = async (
  req: Request<unknown, unknown, Credentials>,
  res: Response,
) => {
  const {email, password} = req.body;
  const user = await createQueryBuilder(UserEntity, 'user')
    .addSelect('user.password')
    .select(['user.password', 'user.email'])
    .where('user.email = :email', {email})
    .getOne();

  if (!user) {
    return res.status(401).json(createHttpError(401, 'email_not_found'));
  }

  const isPasswordValid = await bcrypt.compare(password, user.password);

  if (!isPasswordValid) {
    logger.warning('%s has provided wrong credentials', req.session.user.email);
    return res.status(401).json(createHttpError(401, 'invalid_credentials'));
  }

  req.session.user = await UserEntity.findOne({
    where: {
      email,
    },
    relations: ['role'],
  });
  await req.session.save();
  return res.status(200).end();
};

export default userLogin;
