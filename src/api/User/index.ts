import UserEntity from '@entities/UserEntity';
import authMiddleware from '@middleware/authMiddleware';
import express from 'express';
import {getRepository} from 'typeorm';

import addUserAddress from './User.Address.Create';
import getUserActiveAddress from './User.Address.GetActive';

const user = express.Router();

user.get('/', async (req, res) => {
  const userData = await getRepository(UserEntity)
    .createQueryBuilder('user')
    .where('user.id = :userId', {userId: req.session.user.id})
    .leftJoinAndSelect('user.role', 'role')
    .getOne();

  const result = {
    ...userData,
  };
  res.json(result);
});

user.get('/address', authMiddleware, getUserActiveAddress);
user.post('/address', authMiddleware, addUserAddress);

export default user;
