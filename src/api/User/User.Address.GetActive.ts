import AddressEntity from '@entities/AddressEntity';
import {Request, Response} from 'express';
import {getRepository} from 'typeorm';

const getUserActiveAddress = async (req: Request, res: Response) => {
  const address = await getRepository(AddressEntity)
    .createQueryBuilder('address')
    .leftJoin('address.user', 'user')
    .where('user.id = :userId', {userId: req.session.user?.id})
    .andWhere('address.isActive = :isActive', {isActive: true})
    .getOne();

  res.json(address);
};

export default getUserActiveAddress;
