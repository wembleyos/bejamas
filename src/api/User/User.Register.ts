import UserEntity from '@entities/UserEntity';
import {RegisterData} from '@typings';
import {Request, Response} from 'express';
import createHttpError from 'http-errors';

import bcrypt from 'bcryptjs';
import {omit} from 'lodash';
import RoleEntity from '@entities/RoleEntity';

const userRegister = async (
  req: Request<unknown, unknown, RegisterData>,
  res: Response,
) => {
  const {password, confirmPassword, email, ...data} = req.body;

  const user = await UserEntity.findOne({email});
  if (user) {
    return res
      .status(422)
      .json(createHttpError(422, 'email_already_registered'));
  }

  if (password !== confirmPassword) {
    return res
      .status(422)
      .json(createHttpError(422, {confirmPassword: 'mismatch'}));
  }

  const role = [await RoleEntity.findOne(2)];
  const salted = await bcrypt.hash(password, 10);

  const newUser = UserEntity.create({
    ...data,
    email,
    role,
    password: salted,
  });

  await newUser.save();
  return res.json(omit(newUser, ['password']));
};

export default userRegister;
