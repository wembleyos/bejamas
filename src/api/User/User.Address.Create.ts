import AddressEntity from '@entities/AddressEntity';
import UserEntity from '@entities/UserEntity';
import {Address} from '@typings';
import {Request, Response} from 'express';

const createUserAddress = async (
  req: Request<unknown, unknown, Address>,
  res: Response,
) => {
  const address = await AddressEntity.createQueryBuilder('address')
    .leftJoin('address.user', 'user')
    .where('address.street = :street', {street: req.body.street})
    .andWhere('address.houseNumber = :houseNumber', {
      houseNumber: req.body.houseNumber,
    })
    .andWhere('address.flatNumber = :flatNumber', {
      flatNumber: req.body.flatNumber,
    })
    .andWhere('address.city = :city', {city: req.body.city})
    .andWhere('address.zipCode = :zipCode', {zipCode: req.body.zipCode})
    .andWhere('address.firstName = :firstName', {firstName: req.body.firstName})
    .andWhere('address.lastName = :lastName', {lastName: req.body.lastName})
    .andWhere('user.id = :userId', {userId: req.session.user.id})
    .getOne();

  if (!address) {
    const newAdress = AddressEntity.create(req.body);
    newAdress.user = await UserEntity.findOne(req.session.user.id);
    await newAdress.save();
    return res.json(newAdress);
  }

  await AddressEntity.update(address.id, {
    isActive: req.body.isActive,
  });

  return res.json(address);
};

export default createUserAddress;
