import authMiddleware from '@middleware/authMiddleware';
import validate from '@middleware/validateMiddleware';
import express from 'express';

import {loginSchema, registerSchema} from './schemas';

import user from './User';
import userLogin from './User/User.Login';
import userRegister from './User/User.Register';

import config from './config';
import category from './category';
import product from './product';
import order from './order';

const api = express.Router();

api.use('/order', order);
api.use('/config', config);
api.use('/category', category);
api.use('/product', product);

api.use('/user', authMiddleware, user);
api.post('/register', validate(registerSchema), userRegister);
api.post('/login', validate(loginSchema), userLogin);

export default api;
