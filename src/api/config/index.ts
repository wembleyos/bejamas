import CategoryEntity from '@entities/CategoryEntity';
import CurrencyEntity from '@entities/CurrencyEntity';
import OptionEntity from '@entities/OptionEntity';
import express, {Request, Response} from 'express';

const config = express.Router();

config.get('/', async (req: Request, res: Response) => {
  const currency = await CurrencyEntity.findOne({
    where: {
      id: 1,
    },
  });

  const categories = await CategoryEntity.createQueryBuilder('category')
    .leftJoinAndSelect('category.children', 'children')
    .leftJoinAndSelect('children.children', 'nested')
    .where('category.parent IS NULL')
    .andWhere('category.isRoot = :isRoot', {isRoot: true})
    .getMany();

  const options = await OptionEntity.createQueryBuilder('option')
    .leftJoin('option.parent', 'parent')
    .leftJoinAndSelect('option.children', 'children')
    .where('parent.id IS NULL')
    .getMany();

  res.json({
    currency,
    options,
    categories,
  });
});

export default config;
