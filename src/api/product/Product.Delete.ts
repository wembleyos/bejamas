import ProductEntity from '@entities/ProductEntity';
import {Request, Response} from 'express';
import {container} from '@middleware/logger';
import ImageEntity from '@entities/ImageEntity';

import fs from 'fs';
import path from 'path';
import CategoryEntity from '@entities/CategoryEntity';
import connection from '../../connection';

const logger = container.get('product');

const productDelete = async (
  req: Request<{productId: string}>,
  res: Response,
) => {
  try {
    const {productId} = req.params;
    const product = await (
      await connection
    ).manager.findOne(ProductEntity, productId, {relations: ['images']});

    const filesToDelete = product.images.map(
      (image) =>
        new Promise<ImageEntity>((resolve, reject) =>
          fs.unlink(path.join(process.cwd(), `/public${image.url}`), (err) => {
            if (err) reject(err);
            resolve(image);
          }),
        ),
    );

    const productName = product.name;
    product.images = null;
    product.price = null;
    product.options = null;

    await (await connection)
      .createQueryBuilder()
      .update(CategoryEntity)
      .set({featured: null})
      .where('featured = :id', {id: product.id})
      .execute();

    const images = await Promise.all(filesToDelete);
    await ImageEntity.remove(images);
    await product.remove();

    logger.info(
      'Product %s has been deleted by %s',
      productName,
      req.session.user.email,
    );
    res.status(200).end();
  } catch (e) {
    logger.error('Product cannot be deleted: %s', e.message);
    res.status(422).end();
  }
};

export default productDelete;
