import express from 'express';

import validate from '@middleware/validateMiddleware';
import {productSchema} from '@api/schemas';

import productCreate from './Product.Create';
import productDelete from './Product.Delete';

import {claimRole} from '../../middleware/authMiddleware';
import productChangePotd from './Product.ChangePotd';

const product = express.Router();

product.post(
  '/',
  claimRole(['admin']),
  validate(productSchema.createProduct),
  productCreate,
);

product.post(
  '/potd',
  claimRole(['admin']),
  validate(productSchema.changePotd),
  productChangePotd,
);

product.delete('/:productId', claimRole(['admin']), productDelete);

export default product;
