import ProductEntity from '@entities/ProductEntity';
import PriceEntity from '@entities/PriceEntity';
import CategoryEntity from '@entities/CategoryEntity';

import {Request, Response} from 'express';

import path from 'path';
import fs from 'fs';
import {ProductInput} from '@typings';
import CurrencyEntity from '@entities/CurrencyEntity';
import ImageEntity from '@entities/ImageEntity';
import {container} from '@middleware/logger';
import OptionEntity from '@entities/OptionEntity';
import slugify from 'slugify';

const logger = container.get('product');

const productCreate = async (
  req: Request<unknown, unknown, ProductInput>,
  res: Response,
) => {
  const {name, description, options, ...input} = req.body;

  const matches = (req.body.image as string).match(
    /^data:.+\/(.+);base64,(.*)$/,
  );
  const ext = matches[1];
  const base64 = matches[2];
  const fileName = `${new Date().getTime()}.${ext}`;

  let currency;
  let productOptions;

  try {
    currency = await CurrencyEntity.findOne(req.body.currency);
  } catch (e) {
    logger.error('Unable to fetch currency: %s', e.message);
    return res.status(422).end();
  }

  try {
    productOptions = await OptionEntity.createQueryBuilder('option')
      .where('option.id IN (:...options)', {
        options: [...new Set(Object.values(req.body.options).flat())],
      })
      .getMany();
  } catch (e) {
    logger.error('Unable to fetch options: %s', e.message);
    return res.status(422).end();
  }

  const price = PriceEntity.create({
    value: req.body.price,
    currency,
  });

  let category;
  try {
    category = await CategoryEntity.createQueryBuilder('category')
      .leftJoinAndSelect('category.parent', 'parent')
      .leftJoinAndSelect('parent.parent', 'rParent')
      .leftJoinAndSelect('rParent.parent', 'bParent')
      .where('category.id = :categoryId', {categoryId: req.body.category})
      .getOne();
  } catch (e) {
    logger.error('Unable to fetch product category: %s', e.message);
    return res.status(500).end();
  }

  await price.save();
  const product = ProductEntity.create({
    ...input,
    category,
    name,
    options: productOptions,
    price,
  });

  product.category = category;

  const image = ImageEntity.create({
    name: fileName,
  });

  const categoriesPath = [
    '/',
    category?.parent?.parent?.parent?.slug,
    category?.parent?.parent?.slug,
    category?.parent?.slug,
    category?.slug,
  ].filter(Boolean);

  product.slug = categoriesPath
    .concat(slugify(product.name))
    .join('/')
    .replace(/\/+/g, '/')
    .toLowerCase();

  try {
    await product.save();
  } catch (e) {
    logger.error('Unable to add product: %s', e.message);
    return res.status(500).send();
  }

  try {
    await fs.writeFileSync(
      path.join(process.cwd(), `public/products/${fileName}`),
      Buffer.from(base64, 'base64'),
    );

    image.url = `/products/${fileName}`;
    image.product = product;

    await image.save();
    await product.save();
    logger.info(`Product %s has been added to %s`, product.name, category.id);
    return res.status(200).json(product);
  } catch (e) {
    logger.error('Unable to create product image: %s', e.message);
    return res.status(500).end();
  }
};

export default productCreate;
