import CategoryEntity from '@entities/CategoryEntity';
import ProductEntity from '@entities/ProductEntity';
import logger from '@middleware/logger';
import {ChangePotdArgs} from '@typings';
import {Request, Response} from 'express';

const productChangePotd = async (
  req: Request<unknown, unknown, ChangePotdArgs>,
  res: Response,
) => {
  const data = req.body;

  const product = await ProductEntity.createQueryBuilder('product')
    .where('product.id = :id', {id: data.product})
    .leftJoinAndSelect('product.category', 'category')
    .getOne();

  const category = await CategoryEntity.createQueryBuilder('category')
    .leftJoinAndSelect('category.featured', 'featured')
    .where('category.id = :id', {id: data.category})
    .getOne();

  if (!category) {
    return res.status(400).end();
  }

  if (!data.product) {
    await CategoryEntity.update(category.id, {
      featured: null,
    });
    logger.debug('POTD of %s has been removed', category.name);
    return res.status(200).end();
  }

  try {
    await CategoryEntity.update(category.id, {
      featured: product,
    });
    logger.info('POTD of %s updated with %s', category.name, product.id);
    return res.status(200).json(product);
  } catch (e) {
    logger.error('Cannot update POTD of %s (%s)', category.name, e.message);
    return res.status(500).end(e.message);
  }
};

export default productChangePotd;
