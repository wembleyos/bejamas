import loginSchema from './loginSchema';
import registerSchema from './registerSchema';

import productSchema from './productSchema';

const schemas = {
  loginSchema,
  registerSchema,
  productSchema,
};

export {loginSchema, registerSchema, productSchema};
export default schemas;
