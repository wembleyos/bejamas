import Joi from 'joi';

const loginSchema = {
  email: Joi.string()
    .required()
    .email({tlds: {allow: false}}),
  password: Joi.string().required(),
};

export default loginSchema;
