import Joi from 'joi';

const registerSchema = {
  email: Joi.string()
    .required()
    .email({tlds: {allow: false}}),
  password: Joi.string().min(6).required(),
  confirmPassword: Joi.string().min(6).required(),
  firstName: Joi.string().min(3),
};

export default registerSchema;
