import Joi from 'joi';

const deleteCategory = {
  categoryId: Joi.number().required,
};

const createCategory = {
  name: Joi.string().required().min(4),
  parent: Joi.number(),
  options: Joi.array(),
  isRoot: Joi.boolean(),
};

const categorySchema = {
  deleteCategory,
  createCategory,
};

export default categorySchema;
