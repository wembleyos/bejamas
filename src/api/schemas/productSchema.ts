import Joi from 'joi';

const createProduct = {
  name: Joi.string().required().min(5),
  subtitle: Joi.string(),
  description: Joi.string(),
  price: Joi.number().required(),
  featured: Joi.boolean(),
  category: Joi.number().required(),
  image: Joi.binary().required(),
  currency: Joi.number().required(),
  bestseller: Joi.boolean(),
  options: Joi.object(),
};

const changePotd = {
  category: Joi.number().required(),
  product: Joi.number().required(),
};

const productSchema = {
  createProduct,
  changePotd,
};

export default productSchema;
