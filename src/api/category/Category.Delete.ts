import {Request, Response} from 'express';
import {DeleteCategoryArgs} from '@typings';
import connection from 'src/connection';
import CategoryEntity from '@entities/CategoryEntity';

import {container} from '@middleware/logger';

const logger = container.get('category');

const deleteCategory = async (
  req: Request<{categoryId: string}, unknown, DeleteCategoryArgs>,
  res: Response,
) => {
  try {
    const category = await (
      await connection
    ).manager.findOne(CategoryEntity, req.params.categoryId, {
      relations: ['parent', 'children', 'options'],
    });

    const childrens = category?.children;

    if (category) {
      category.options = null;
      category.products = null;
      category.children = null;
      category.parent = null;
      await category.save();
      await CategoryEntity.remove(childrens);
      await category.remove();
    }

    logger.info('Category %s has been removed');
    return res.status(200).end();
  } catch (e) {
    logger.error(
      'Unable to delete category %s: %s',
      req.params.categoryId,
      e.message,
    );
    return res.status(500).end();
  }
};

export default deleteCategory;
