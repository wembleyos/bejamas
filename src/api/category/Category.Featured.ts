import CategoryEntity from '@entities/CategoryEntity';
import ProductEntity from '@entities/ProductEntity';
import {Request, Response} from 'express';
import {container} from '@middleware/logger';
import {flatten} from './Category.Single';

const logger = container.get('category');

const categoryFeatured = async (
  req: Request<{categoryId: string}>,
  res: Response,
) => {
  try {
    const category = await CategoryEntity.createQueryBuilder('category')
      .leftJoinAndSelect('category.children', 'children')
      .leftJoinAndSelect('children.children', 'nested')
      .leftJoinAndSelect('nested.children', 'nestedMost')
      .where('category.id = :cId', {cId: req.params.categoryId})
      .getOne();

    if (!category) {
      return res.status(404).end();
    }
    const childrens = flatten(category?.children)
      .flat()
      .concat(category)
      .map(({id}) => id);

    const products = await ProductEntity.createQueryBuilder('product')
      .leftJoinAndSelect('product.category', 'category')
      .leftJoinAndSelect('product.images', 'image')
      .where('category.id IN (:...childrens)', {childrens})
      .getMany();

    return res.json(products);
  } catch (e) {
    logger.error(
      'Unable to fetch featureds for %s (%s)',
      req.params.categoryId,
      e.message,
    );
    return res.status(500).end();
  }
};

export default categoryFeatured;
