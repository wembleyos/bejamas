import CategoryEntity from '@entities/CategoryEntity';
import OptionEntity from '@entities/OptionEntity';
import ProductEntity from '@entities/ProductEntity';
import logger from '@middleware/logger';
import {CategoryMatch, CategorySearchParams} from '@typings';
import {Request, Response} from 'express';
import {omit} from 'lodash';
import {Brackets, getRepository} from 'typeorm';

export const flatten = (children?: CategoryEntity[]): CategoryEntity[] =>
  children?.reduce((acc, r) => {
    if (r.children && r.children.length) {
      return acc.concat(flatten(r.children));
    }
    return acc.concat(r);
  }, []);

const singleCategory = async (
  req: Request<CategoryMatch, unknown, unknown, CategorySearchParams>,
  res: Response,
) => {
  const search = req.query;

  const take = req.session.user?.role?.some((role) => role.name === 'admin')
    ? 5
    : 6;
  const offset = ((search.paginate as unknown as number) - 1) * take || 0;

  const categoryQb = await getRepository(CategoryEntity)
    .createQueryBuilder('category')
    .leftJoinAndSelect('category.children', 'children')
    .leftJoinAndSelect('category.parent', 'parent')
    .leftJoin('category.featured', 'featured')
    .leftJoin('featured.images', 'image')
    .leftJoin('category.options', 'options', 'options.parent IS NULL')
    .leftJoin('options.children', 'values')
    .select([
      'category',
      'children',
      'parent',
      'featured',
      'image',
      'options',
      'values',
      'category.name',
      'category.id',
    ]);

  let rootCategory;
  let subCategory;
  let lastCategory;

  try {
    rootCategory = await categoryQb
      .where('category.slug = :rootSlug', {rootSlug: req.params.category})
      .orWhere('category.id = :rootId', {rootId: req.params.category})
      .getOne();

    subCategory = await categoryQb
      .where('category.slug = :subSlug', {
        subSlug: req.params.subcategory,
      })
      .orWhere('category.slug = :subId', {
        subId: req.params.subcategory,
      })
      .getOne();

    lastCategory = await categoryQb
      .where('category.slug = :lastCategory', {
        lastCategory: req.params.subcategory,
      })
      .orWhere('category.slug = :lastCategory', {
        lastCategory: req.params.lastCategory,
      })
      .getOne();
  } catch (e) {
    logger.error(
      'Cannot fetch category %s (%s)',
      req.params.category,
      e.message,
    );
    return res.status(404).end();
  }

  if (req.params.lastCategory || req.params.subcategory) {
    categoryQb.where('category.id = :category', {
      category: lastCategory?.id || subCategory?.id,
    });
  } else {
    categoryQb.where('category.slug = :rootSlug', {
      rootSlug: req.params.category,
    });
  }

  const productQb = ProductEntity.createQueryBuilder('product');

  if (!rootCategory) {
    const options = await OptionEntity.createQueryBuilder('option')
      .leftJoinAndSelect('option.parent', 'parent')
      .leftJoinAndSelect('option.children', 'children')
      .where('option.parent IS NULL')
      .getMany();
    const products = await productQb.getMany();
    return res.status(200).json({options, products});
  }

  productQb
    .leftJoin('product.category', 'category')
    .leftJoin('category.parent', 'parentCategory')
    .leftJoin('parentCategory.parent', 'rootCategory')
    .leftJoin('product.price', 'price')
    .leftJoin('product.images', 'image')
    .leftJoin('product.options', 'option')
    .leftJoin('price.currency', 'currency')
    .select([
      'product',
      'category',
      'parentCategory',
      'rootCategory',
      'price',
      'currency',
      'image',
      'option',
    ])
    .orderBy('product.id', 'DESC');

  if (search.category) {
    const searchCategories = (
      await CategoryEntity.createQueryBuilder('category')
        .leftJoinAndSelect('category.children', 'children')
        .whereInIds(search.category)
        .getMany()
    )
      .map((item) => (item.children.length ? item.children : item))
      .flat()
      .map(({id}) => id);

    searchCategories.forEach((searchCategory) => {
      productQb.where(
        new Brackets((qb) =>
          qb.andWhere('category.id = :searchCategory', {
            searchCategory,
          }),
        ),
      );
    });
  } else if (subCategory || rootCategory) {
    const categories = flatten(subCategory?.children || rootCategory?.children)
      .map(({id}) => id)
      .concat(subCategory?.id || rootCategory?.id)
      .filter(Boolean);

    productQb.where('category.id IN (:...categories)', {
      categories,
    });
  }

  if (search.price_from) {
    productQb.andWhere('price.value >= :from', {
      from: Number(search.price_from),
    });
  }

  if (search.price_to) {
    productQb.andWhere('price.value <= :to', {to: Number(search.price_to)});
  }

  if (search.price_order) {
    productQb.orderBy(
      'price.value',
      `${search?.price_order}`.toUpperCase() as 'ASC' | 'DESC',
    );
  }
  const category = await categoryQb.getOne();

  const options = Object.values(
    omit(
      search,
      'category',
      'price_to',
      'price_from',
      'paginate',
      'price_order',
    ),
  ).flat();

  if (options.length) {
    for (let i = 0; i < options.length; i += 1) {
      productQb.andWhere(
        new Brackets((qb) => qb.where('option.id = :item', {item: options[i]})),
      );
    }
  }

  const total = await productQb.getCount();

  if (search.paginate) {
    productQb.skip(offset).take(take);
  }

  const products = await productQb.getMany();

  return res.json({...category, products, total});
};

export default singleCategory;
