import categorySchema from '@api/schemas/categorySchema';
import {claimRole} from '@middleware/authMiddleware';
import validate from '@middleware/validateMiddleware';
import express from 'express';

import createCategory from './Category.Create';
import deleteCategory from './Category.Delete';

import categoryOptions from './Category.Options';
import categoryOptionsUpdate from './Category.Options.Update';

import singleCategory from './Category.Single';
import categoryFeatured from './Category.Featured';
import categoryOptionsDelete from './Category.Options.Delete';

const category = express.Router();

category.post(
  '/',
  claimRole(['admin']),
  validate(categorySchema.createCategory),
  createCategory,
);
category.delete('/:categoryId', claimRole(['admin']), deleteCategory);

category.delete(
  '/option/:categoryId/:optionId',
  claimRole(['admin']),
  categoryOptionsDelete,
);

category.patch('/options', claimRole(['admin']), categoryOptionsUpdate);

category.get('/options', categoryOptions);
category.get('/featured/:categoryId', categoryFeatured);
category.get('/:category?/:subcategory?/:lastCategory?', singleCategory);

export default category;
