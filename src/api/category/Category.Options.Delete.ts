import CategoryEntity from '@entities/CategoryEntity';
import {Request, Response} from 'express';

import {container} from '@middleware/logger';
import OptionEntity from '@entities/OptionEntity';
import connection from '../../connection';

const logger = container.get('category');

type DeleteOptionArgs = {
  categoryId: string;
  optionId: string;
};

const categoryOptionsDelete = async (
  req: Request<DeleteOptionArgs, unknown, unknown>,
  res: Response,
) => {
  const input = req.params;
  const option = await OptionEntity.findOne(input.optionId);

  const category = await (
    await connection
  ).manager.findOne(CategoryEntity, input.categoryId, {relations: ['options']});

  if (!option || !category) {
    return res.status(422).end();
  }

  try {
    category.options = category.options.filter((item) => item.id !== option.id);
    await category.save();
    logger.info(
      'Option %s for category %s has been removed',
      option.name,
      category.name,
    );
    return res.status(200).end();
  } catch (e) {
    logger.error(
      'Cannot delete option %s for category %s (%s)',
      option.name,
      category.name,
      e.message,
    );

    return res.status(500).end();
  }
};

export default categoryOptionsDelete;
