import CategoryEntity from '@entities/CategoryEntity';
import OptionEntity from '@entities/OptionEntity';
import {NewCategoryArgs} from '@typings';
import {Request, Response} from 'express';
import slugify from 'slugify';
import {container} from '@middleware/logger';

const logger = container.get('category');

const createCategory = async (
  req: Request<unknown, unknown, NewCategoryArgs>,
  res: Response,
) => {
  const {name} = req.body;
  try {
    const parent = await CategoryEntity.createQueryBuilder('category')
      .where('category.id = :parentId', {parentId: req.body.parent})
      .getOne();

    const options = await OptionEntity.createQueryBuilder('option')
      .leftJoin('option.parent', 'parent')
      .where('option.id IN (:...optionsIds)', {optionsIds: req.body.options})
      .andWhere('option.parent IS Null')
      .getMany();

    const category = CategoryEntity.create({
      name,
      slug: slugify(name).toLowerCase(),
      parent: req.body.isRoot ? null : parent,
      isRoot: !!req.body.isRoot,
      options,
    });
    await category.save();
    res.status(200).json(category);
    logger.info(
      'Category %s has been added to %s with options %s',
      category.name,
      category.parent.name,
      category.options.map((item) => item.name).join(','),
    );
  } catch (e) {
    res.status(402).end();
  }
};

export default createCategory;
