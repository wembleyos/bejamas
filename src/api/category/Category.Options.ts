import ProductEntity from '@entities/ProductEntity';
import {Request, Response} from 'express';

const categoryOptions = async (req: Request, res: Response) => {
  const qb = await ProductEntity.createQueryBuilder('product');
  const query = qb.leftJoinAndSelect('product.options', 'option');

  res.json(await query.getMany());
};

export default categoryOptions;
