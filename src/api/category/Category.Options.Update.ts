import CategoryEntity from '@entities/CategoryEntity';
import OptionEntity from '@entities/OptionEntity';
import {UpdateCategoryOptionsArgs} from '@typings';
import {Request, Response} from 'express';

import {container} from '@middleware/logger';

const logger = container.get('category');

const categoryOptionsUpdate = async (
  req: Request<unknown, unknown, UpdateCategoryOptionsArgs>,
  res: Response,
) => {
  const options = await OptionEntity.createQueryBuilder('option')
    .whereInIds(req.body.options)
    .getMany();

  const category = await CategoryEntity.createQueryBuilder('category')
    .where('category.id = :updateId', {updateId: req.body.categoryId})
    .getOne();

  if (category) {
    try {
      category.options = options;
      await category.save();
      logger.info(
        'Category %s has received options',
        category.name,
        options.map((option) => option.name).join(','),
      );
      return res.status(200).end();
    } catch (e) {
      logger.error(
        'Unable to apply category options for %s (%s)',
        category.name,
        e.message,
      );
      return res.status(500).end();
    }
  }

  return res.status(400).end();
};

export default categoryOptionsUpdate;
