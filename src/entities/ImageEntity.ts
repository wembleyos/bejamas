import {
  BaseEntity,
  Column,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import ProductEntity from './ProductEntity';

@Entity({name: 'image'})
class ImageEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: string;

  @Column({nullable: true})
  alt: string;

  @Column({select: false})
  name: string;

  @Column()
  url: string;

  @ManyToOne(() => ProductEntity, (product) => product.images)
  product: ProductEntity;
}

export default ImageEntity;
