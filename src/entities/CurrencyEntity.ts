import {
  BaseEntity,
  Column,
  Entity,
  ObjectID,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity({name: 'currency'})
class CurrencyEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: ObjectID;

  @Column({unique: true})
  name: string;
}

export default CurrencyEntity;
