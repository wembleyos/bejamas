import {
  BaseEntity,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  ObjectID,
  PrimaryGeneratedColumn,
} from 'typeorm';
import ProductEntity from './ProductEntity';
import PromoCodeEntity from './PromoCodeEntity';
import AddressEntity from './AddressEntity';
import OptionEntity from './OptionEntity';

@Entity({name: 'order'})
class OrderEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: ObjectID;

  @ManyToOne(() => AddressEntity, (address) => address.orderShipping)
  shippingAdress: AddressEntity;

  @ManyToOne(() => AddressEntity, (address) => address.orderBilling)
  billingAddress: AddressEntity;

  @ManyToMany(() => ProductEntity, (product) => product.order)
  @JoinTable({name: 'order_products'})
  products: ProductEntity[];

  @ManyToMany(() => OptionEntity, (option) => option.product)
  @JoinTable({name: 'order_products_options'})
  productOptions: ProductEntity[];

  @ManyToMany(() => PromoCodeEntity, (promo) => promo.order)
  @JoinTable({name: 'order_promocodes'})
  promoCode: PromoCodeEntity[];
}

export default OrderEntity;
