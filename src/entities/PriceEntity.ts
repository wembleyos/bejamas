import {
  BaseEntity,
  Column,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import CurrencyEntity from './CurrencyEntity';

@Entity({name: 'price'})
class PriceEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: string;

  @Column()
  value: number;

  @ManyToOne(() => CurrencyEntity)
  currency: CurrencyEntity;
}

export default PriceEntity;
