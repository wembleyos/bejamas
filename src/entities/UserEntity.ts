import {
  BaseEntity,
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  ObjectID,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import RoleEntity from './RoleEntity';
import ShippingAddressEntity from './AddressEntity';

@Entity('user')
class UserEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: ObjectID;

  @Column()
  email: string;

  @Column({select: false})
  password: string;

  @ManyToMany(() => RoleEntity)
  @JoinTable({name: 'user_roles'})
  role: RoleEntity[];

  @OneToMany(() => ShippingAddressEntity, (address) => address.user, {
    nullable: true,
  })
  address: ShippingAddressEntity[];
}

export default UserEntity;
