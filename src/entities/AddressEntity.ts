import {AddressType} from '@typings';
import {
  BaseEntity,
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  ObjectID,
  PrimaryGeneratedColumn,
} from 'typeorm';
import OrderEntity from './OrderEntity';
import UserEntity from './UserEntity';

@Entity({name: 'address'})
class AddressEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: ObjectID;

  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @Column()
  city: string;

  @Column()
  street: string;

  @Column()
  houseNumber: string;

  @Column({nullable: true})
  flatNumber: string;

  @Column()
  zipCode: string;

  @Column({nullable: true})
  country: string;

  @Column()
  phoneNumber: number;

  @Column()
  type: AddressType;

  @Column({nullable: true})
  company: string;

  @Column({nullable: true})
  taxNumber: string;

  @ManyToOne(() => UserEntity, (user) => user.address)
  user: UserEntity;

  @Column({nullable: true})
  isActive: boolean;

  @ManyToMany(() => OrderEntity, (order) => order.shippingAdress)
  orderShipping: OrderEntity[];

  @ManyToMany(() => OrderEntity, (order) => order.shippingAdress)
  orderBilling: OrderEntity[];
}

export default AddressEntity;
