import {
  BaseEntity,
  Column,
  CreateDateColumn,
  Entity,
  JoinColumn,
  JoinTable,
  ManyToMany,
  ManyToOne,
  ObjectID,
  OneToMany,
  OneToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import CategoryEntity from './CategoryEntity';
import ImageEntity from './ImageEntity';
import OptionEntity from './OptionEntity';
import OrderEntity from './OrderEntity';
import PriceEntity from './PriceEntity';
import UserEntity from './UserEntity';

@Entity({name: 'product'})
class ProductEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: ObjectID;

  @Column()
  name: string;

  @Column()
  slug: string;

  @Column({nullable: true})
  subtitle: string;

  @Column({select: false, nullable: true})
  sku: number;

  @Column({nullable: true})
  bestseller: boolean;

  @Column({nullable: true})
  featured: boolean;

  @OneToMany(() => ImageEntity, (image) => image.product, {
    cascade: true,
  })
  @JoinColumn()
  images: ImageEntity[];

  @Column({nullable: true})
  description?: string;

  @Column({nullable: true})
  details?: string;

  @ManyToOne(() => CategoryEntity, (category) => category.id)
  category: CategoryEntity;

  @CreateDateColumn({nullable: true})
  createdAt?: Date;

  @UpdateDateColumn({nullable: true})
  updatedAt?: Date;

  @OneToOne(() => UserEntity)
  createdBy: UserEntity;

  @ManyToOne(() => PriceEntity, {
    cascade: true,
  })
  price: PriceEntity;

  @ManyToMany(() => OptionEntity, (option) => option.product)
  @JoinTable({name: 'product_options'})
  options: OptionEntity[];

  @ManyToMany(() => OrderEntity, (order) => order.products, {cascade: true})
  @JoinTable({name: 'order_products'})
  order: OptionEntity[];

  @ManyToMany(() => OrderEntity, (order) => order.productOptions)
  @JoinTable({name: 'order_products_options'})
  orderProductsOptions: OptionEntity[];
}

export default ProductEntity;
