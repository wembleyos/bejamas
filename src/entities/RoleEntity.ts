import {
  BaseEntity,
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  ObjectID,
  PrimaryGeneratedColumn,
} from 'typeorm';
import UserEntity from './UserEntity';

@Entity({name: 'role'})
class RoleEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: ObjectID;

  @Column({select: false})
  scope: string;

  @Column()
  name: string;

  @ManyToMany(() => UserEntity, (user) => user.role)
  @JoinTable({name: 'user_roles'})
  user: UserEntity[];
}

export default RoleEntity;
