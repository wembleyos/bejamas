import {
  BaseEntity,
  Column,
  Entity,
  ObjectID,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity({name: 'country'})
class CountryEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: ObjectID;

  @Column({unique: true})
  code: string;

  @Column()
  name: string;
}

export default CountryEntity;
