import {
  BaseEntity,
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  ObjectID,
  OneToMany,
  PrimaryGeneratedColumn,
} from 'typeorm';
import CategoryEntity from './CategoryEntity';
import ProductEntity from './ProductEntity';

@Entity({name: 'option'})
class OptionEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: ObjectID;

  @Column()
  value: string;

  @Column({nullable: true})
  descriptor?: string;

  @Column()
  name: string;

  @ManyToOne(() => OptionEntity, (option) => option.children)
  parent: OptionEntity;

  @OneToMany(() => OptionEntity, (option) => option.parent)
  children?: OptionEntity[];

  @ManyToMany(() => CategoryEntity)
  @JoinTable({name: 'category_options'})
  category: CategoryEntity[];

  @ManyToMany(() => ProductEntity)
  @JoinTable({name: 'product_options'})
  product: ProductEntity[];
}

export default OptionEntity;
