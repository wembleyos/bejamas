import {
  Entity,
  BaseEntity,
  Column,
  PrimaryGeneratedColumn,
  ManyToOne,
  OneToMany,
  ManyToMany,
  JoinTable,
  JoinColumn,
} from 'typeorm';
import OptionEntity from './OptionEntity';
import ProductEntity from './ProductEntity';

@Entity({name: 'category'})
class CategoryEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: string;

  @Column()
  name: String;

  @Column()
  slug: string;

  @Column()
  isRoot: boolean;

  @ManyToOne(() => ProductEntity)
  featured: ProductEntity;

  @OneToMany(() => CategoryEntity, (category) => category.parent, {
    cascade: true,
  })
  children?: CategoryEntity[];

  @ManyToOne(() => CategoryEntity, (category) => category.children)
  @JoinColumn()
  parent: CategoryEntity;

  @OneToMany(() => ProductEntity, (product) => product.category)
  products: ProductEntity[];

  @ManyToMany(() => OptionEntity)
  @JoinTable({name: 'category_options'})
  options: OptionEntity[];
}

export default CategoryEntity;
