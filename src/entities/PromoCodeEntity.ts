import {
  BaseEntity,
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  ObjectID,
  PrimaryGeneratedColumn,
} from 'typeorm';
import OrderEntity from './OrderEntity';

@Entity({name: 'promocode'})
class PromoCodeEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: ObjectID;

  @Column({unique: true})
  code: string;

  @ManyToMany(() => OrderEntity, (order) => order.promoCode, {nullable: true})
  @JoinTable({name: 'order_promocodes'})
  order: OrderEntity;
}

export default PromoCodeEntity;
