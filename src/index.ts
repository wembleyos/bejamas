import 'reflect-metadata';

import dotenv from 'dotenv';
import express from 'express';

import path from 'path';
import middlewares from '@middleware/index';
import {devMiddleware, hotMiddleware} from '@middleware/webpackMiddleware';
import htmlRenderer from '@middleware/htmlRenderer';
import logger from '@middleware/logger';
import api from './api';
import connection from './connection';

dotenv.config();

const app = express();
app.set('env', process.env.NODE_ENV);

middlewares.forEach((middleware) => app.use(middleware));
app.use(express.static(path.join(process.cwd(), 'public')));

app.use((req, res, next) => {
  // eslint-disable-next-line no-underscore-dangle
  res.locals._csrf = req.csrfToken();
  next();
});

if (app.get('env') === 'production') {
  app.use(express.static(path.join(process.cwd(), 'dist')));
  app.set('trust proxy', 1);
}

app.use('/api', api);

const start = async () => {
  logger.debug('Establishing database connection');
  try {
    await connection;
    logger.debug('Connection established');
  } catch (e) {
    logger.error('Error during establishing connection (%s)', e.message);
    process.kill(process.pid, 'SIGTERM');
  }
  app.use(devMiddleware);

  if (app.get('env') === 'development') {
    logger.info('Using development environment');
    app.use(hotMiddleware);
  } else {
    logger.info('Using production environment');
  }

  devMiddleware.waitUntilValid(() => {
    app.get('/*', htmlRenderer);
    app.listen(process.env.PORT, () =>
      logger.info(`Listening on http://localhost:${process.env.PORT}`),
    );
  });
};

start();
