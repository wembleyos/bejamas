import {createConnection} from 'typeorm';
import dotenv from 'dotenv';
import path from 'path';

dotenv.config();

const connection = createConnection({
  type: process.env.TYPEORM_CONNECTION as any,
  host: process.env.TYPEORM_HOST,
  username: process.env.TYPEORM_USERNAME,
  password: process.env.TYPEORM_PASSWORD,
  database: process.env.TYPEORM_DATABASE,
  port: Number(process.env.TYPEORM_PORT),
  synchronize: process.env.TYPEORM_SYNCHRONIZE === 'true',
  logging: process.env.TYPEORM_LOGGING === 'true',
  entities: [path.join(process.cwd(), 'src/entities/*.ts')],
});

export default connection;
