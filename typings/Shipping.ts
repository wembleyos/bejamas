export type AddressType = 'shipping' | 'billing';

export interface Country {
  id: string;
  code: string;
  name: string;
}

export interface Address {
  id?: string;
  firstName: string;
  lastName: string;
  city: string;
  street: string;
  houseNumber: string;
  flatNumber?: string;
  zipCode: string;
  phoneNumber: number;
  company?: string;
  taxNumber?: string;
  isActive?: boolean;
  country?: string;
  type?: AddressType;
}
