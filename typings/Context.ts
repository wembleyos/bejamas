import {StaticContext} from 'react-router';
import {User} from './User';

export interface Context extends StaticContext {
  url?: string;
  user?: User;
}
