import {Option} from './Filters';
import {Product} from './Product';

export interface Category {
  id: string;
  name: string;
  parent?: Category;
  children?: Category[];
  slug?: string;
  products?: Product[];
  featured?: Product;
  total?: number;
  recommendations: Product[];
  options: Option[];
}

export interface CategoryMatch {
  category: string;
  subcategory: string;
  lastCategory: string;
}

export interface CategorySearchParams {
  [key: string]: string | string[];
}

export interface DeleteCategoryArgs {
  categoryId: string;
}

export interface DeleteCategoryOptionArgs {
  categoryId: string;
  optionId: string;
}

export interface UpdateCategoryOptionsArgs {
  categoryId?: string;
  options: string[];
}

export interface NewCategoryArgs {
  name: string;
  parent: string;
  isRoot?: boolean;
  options?: string[];
}
