export interface Language {
  id: string;
  key: string;
}
