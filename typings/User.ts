export interface Role {
  id: string;
  scope: string;
  name: string;
  description?: string;
}

export interface User {
  id: string;
  email: string;
  role: Role[];
}

export interface Credentials {
  email: string;
  password: string;
}

export interface RegisterData extends Credentials {
  confirmPassword: string;
  firstName: string;
}
