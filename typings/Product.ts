import {OptionsSelectPayload} from '@components/OptionsWrapper';
import {Category} from './Category';
import {Option} from './Filters';

export type Currency = {
  id: string;
  name: string;
};

export interface Price {
  id?: string;
  value: string;
  currency: Currency;
}

export interface ProductImage {
  id: string;
  alt: string;
  url: string;
}

export interface Product {
  id: string;
  price: Price;
  category: Category;
  name: string;
  subtitle?: string;
  description: string;
  slug: string;
  details: string;
  featured: boolean;
  bestseller?: boolean;
  currency: Currency;
  images: ProductImage[];
  recommendations?: Product[];
  options: Option[];
}

export interface ProductInput
  extends Omit<
    Product,
    'options' | 'image' | 'currency' | 'price' | 'category'
  > {
  image: string | ArrayBuffer;
  price: number;
  currency: string;
  category: string;
  options: OptionsSelectPayload;
}

export interface ChangePotdArgs {
  product: string;
  category: string;
}
