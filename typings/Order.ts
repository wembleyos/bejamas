import {Product} from './Product';
import {Address} from './Shipping';

export interface PlaceOrderArgs {
  billingAddress: Address;
  shippingAddress: Address;
  products: Product[];
}
