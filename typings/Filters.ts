export type FiltersCategoryType = string[];

export interface FiltersPriceType {
  price_from?: number;
  price_to?: number;
}

export interface Filters {
  category?: string[];
  price?: FiltersPriceType;
}

export interface Option {
  id: string;
  name: string;
  value: string;
  descriptor?: string;
  parent?: Option;
  children?: Option[];
}
